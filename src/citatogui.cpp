#include "citatogui.h"
#include <QApplication>
#include <QClipboard>
#include <QMessageBox>
#include <QMimeData>
#include <QSettings>
#include <QDebug>

CitatoGui::CitatoGui(QString version, QWidget *parent)
    : QMainWindow(parent)
{
    CITATOVERSION = version;
    setMinimumSize(650, 570);
    setAcceptDrops(true);

    setupCentralZone();
    setupStyle();
    setupLayout();
    setupMenu();
    setupSignalsSlots();
    setupKeyboardShortcuts();

    addAuthorField();
    addEditorField();
    addTranslatorField();

    listOfDocumentsBox->setCurrentIndex(1);		// always set the document at book in startup
    loadSettings();	// load settings if available

    if(listOfLanguagesBox->currentIndex() == 0) {	// make sure that citato starts with a defined language
        listOfLanguagesBox->setCurrentIndex(1);
    }
    if(listOfFormatsBox->currentIndex() == 0){
        listOfFormatsBox->setCurrentIndex(1);
    }

    // TEST: replace the date of last time checked for update
    QSettings settings("Citato_experimental", "citato_main_experimental");
    settings.beginGroup("update");
    settings.setValue("last checked update", QVariant(QDate::fromString("1MM12car2003", "d'MM'MMcaryyyy")));
    settings.endGroup();

    verifyLastUpdate();	// verify when the last time we checked for update

    // for test purpse
//    std::unique_ptr<QFile> a(new QFile("/home/szinedine/en.citato"));
//    std::unique_ptr<QJsonDocument> aa(new QJsonDocument(CitatoFile::fromFileToJsonDocument(a.release())));
//    displayDocument(aa.release());

//    listOfDocumentsBox->setCurrentIndex(4);
}

void CitatoGui::constructElement(){

    allocElement();

    element->setDocumentLanguage(listOfLanguagesBox->currentIndex());
    element->setDocumentType(listOfDocumentsBox->currentIndex());
    element->setAuthors(fromNameWidgetListToNameComponentsList(authorsNameWidgetList));
    element->setEditors(fromNameWidgetListToNameComponentsList(editorsNameWidgetList));
    element->setTranslators(fromNameWidgetListToNameComponentsList(translatorsNameWidgetList));
    element->setTitle(title->getText());
    element->setDissertationType(dissertationTypeBox->currentIndex());
    element->setInstitution(institution->getText());
    element->setChapter(chapter->getText());
    element->setJournal(journal->getText());
    element->setIssueNumberJournal(issueNumberForJournal->getText());
    element->setVolumNumber(volumNumber->getText());
    element->setVolumTitle(volumTitle->getText());
    element->setEditionNumber(editionNumber->getText());
    element->setLocation(countryOfPublication->getText());
    element->setEditionsHome(editionsHome->getText());
    element->setYearOfPublication(dateOfPublication->getYear());
    element->setMonthOfPublication(dateOfPublication->getMonth());
    element->setDayOfPublication(dateOfPublication->getDay());
    element->setCurrentPage(currentPage->getText());
    element->setPageFrom(pages->getFromText());
    element->setPageTo(pages->getToText());
    element->setWebSiteName(webSiteName->getText());
    element->setWebSiteUrl(webSiteUrl->getText());
//    element->setAccessDate(accessDate->getText());
    element->setAccessYear(accessDate->getYear());
    element->setAccessMonth(accessDate->getMonth());
    element->setAccessDay(accessDate->getDay());
    element->setDOI(DOI->getText());
    element->setISBN(ISBN->getText());
}

void CitatoGui::callComposer(){
    if(listOfLanguagesBox->currentIndex() == 0 || listOfDocumentsBox->currentIndex() == 0){
        QMessageBox::warning(this, "Sélectionnez une langue et un type de document",
                            "Veillez sélectionner une langue et un type de document (livre, article...).");
        return;
    }

    constructElement();

    // call the test class to verify if enough fields have been filled
    std::unique_ptr<TestDocument> t(new TestDocument(*element, listOfDocumentsBox->currentIndex(),
                            listOfLanguagesBox->currentIndex(), this));
    if(t->isError()){
        QMessageBox::warning(this, "not enough text input", t->getErrorMessage());
        return;
    }

    if(chicagoFootnoteStyleAction->isChecked()){
        composer = new ChicagoFootnoteStyle (*element,
                                             listOfDocumentsBox->currentIndex(),
                                             listOfLanguagesBox->currentIndex(),
                                             listOfFormatsBox->currentIndex());
    }
    else if(chicagoInTextStyleAction->isChecked()){
        composer = new ChicagoInTextStyle (*element,
                                             listOfDocumentsBox->currentIndex(),
                                             listOfLanguagesBox->currentIndex(),
                                             listOfFormatsBox->currentIndex());
    }
    else{
        composer = new ChicagoFootnoteStyle (*element,
                                             listOfDocumentsBox->currentIndex(),
                                             listOfLanguagesBox->currentIndex(),
                                             listOfFormatsBox->currentIndex());
    }

    citationString = composer->getCitation().simplified();
    bibliographyString = composer->getBibliography().simplified();

    citationDisplayer->setText(citationString);
    bibliographyDisplayer->setText(bibliographyString);

    freeComposer();
    freeElement();
}


void CitatoGui::addAuthorField(){

    NameWidget *singleAuthor = new NameWidget(listOfLanguagesBox->currentIndex(), authorsNameWidgetList.size()+1,
                                              NameWidget::author, 110, this);
    if(authorsNameWidgetList.isEmpty()){
        singleAuthor->getLayout()->addWidget(addAuthorsButton);
        singleAuthor->getLayout()->addWidget(deleteAuthorButton);
    }

    authorsNameWidgetList.append(singleAuthor);
    authorsWidgetsLayout->addWidget(singleAuthor);
}


void CitatoGui::addEditorField(){

    NameWidget *singleEditor = new NameWidget(listOfLanguagesBox->currentIndex(), editorsNameWidgetList.size()+1,
                                              NameWidget::editor, 110, this);

    if(editorsNameWidgetList.isEmpty()){
        singleEditor->getLayout()->addWidget(addEditorsButton);
        singleEditor->getLayout()->addWidget(deleteEditorButton);
    }

    editorsNameWidgetList.append(singleEditor);
    editorsWidgetsLayout->addWidget(singleEditor);
}


void CitatoGui::addTranslatorField(){

    NameWidget *singleTranslator = new NameWidget(listOfLanguagesBox->currentIndex(), translatorsNameWidgetList.size()+1,
                                              NameWidget::translator, 110, this);
    if(translatorsNameWidgetList.isEmpty()){
        singleTranslator->getLayout()->addWidget(addTranslatorButton);
        singleTranslator->getLayout()->addWidget(deleteTranslatorButton);
    }

    translatorsNameWidgetList.append(singleTranslator);
    translatorsWidgetsLayout->addWidget(singleTranslator);
}


void CitatoGui::deleteAuthorField()
{
    if(authorsNameWidgetList.size() == 1) { // if there is only one widget, just clear it. don't delete it.
        authorsNameWidgetList.at(0)->clear();
        return;
    }
    delete authorsNameWidgetList.last();
    authorsNameWidgetList.removeLast();
}

void CitatoGui::deleteEditorField()
{
    if(editorsNameWidgetList.size() == 1) {
        editorsNameWidgetList.at(0)->clear();
        return;
    }
    delete editorsNameWidgetList.last();
    editorsNameWidgetList.removeLast();
}

void CitatoGui::deleteTranslatorField()
{
    if(translatorsNameWidgetList.size() == 1) {
        translatorsNameWidgetList.at(0)->clear();
        return;
    }
    delete translatorsNameWidgetList.last();
    translatorsNameWidgetList.removeLast();
}



void CitatoGui::copyCitation(){
    if(citationString.isEmpty()){
        QMessageBox::warning(this, "Error",
                             "Erreur, Vous devez générer d'abord la citation et l'entrée bibliographique avant de les copier."
                             "\nVeillez appuyer d'abord sur le bouton Générer.");
        return;
    }

    QClipboard *clip = QApplication::clipboard();
    QMimeData *richText = new QMimeData;
    richText->setHtml(citationString);
    clip->setMimeData(richText);
}

void CitatoGui::copyBibliography(){
    if(citationString.isEmpty()){
        QMessageBox::warning(this, "Error",
                             "Erreur, Vous devez générer d'abord la citation et l'entrée bibliographique avant de les copier."
                             "\nVeillez appuyer d'abord sur le bouton Générer.");
        return;
    }

    QClipboard *clip = QApplication::clipboard();
    QMimeData *richText = new QMimeData;
    richText->setHtml(bibliographyString);
    clip->setMimeData(richText);
}


QList<NameComponents> CitatoGui::fromNameWidgetListToNameComponentsList(QList<NameWidget*> lst)
{
    QList<NameComponents> final = {};
    for(int i = 0 ; i < lst.size() ; ++i){
        if(lst.at(i)->isValid()) { final.append(lst.at(i)->getName()); }
    }
    return final;
}


void CitatoGui::setupCentralZone(){

    mainWidget = new QWidget(this);
    mainLayout = new QGridLayout;
    mainWidget->setLayout(mainLayout);
    setCentralWidget(mainWidget);

    fieldsContainerScrollArea = new QScrollArea(this);
    widgetOfScrollArea = new QWidget(this);
    fieldsContainerScrollArea->setWidget(widgetOfScrollArea);
    fieldsContainerScrollArea->setWidgetResizable(true);

    // buttons and comboBoxes in the header (first row)
    clearAllButton = new QPushButton("Effacer tout", this);
    generateButton = new QPushButton("Générer", this);
    listOfDocumentsBox = new QComboBox(this);
    listOfLanguagesBox = new QComboBox(this);
    dissertationTypeBox = new QComboBox(this);
    listOfFormatsBox = new QComboBox(this);
    listOfFormatsBox->setMaximumWidth(130);

    // comboBox
    QStringList lng;
    lng << "Choisissez une Langue"
          << "English"
          << "Français"
          << "العربية"
             ;

    listOfLanguagesBox->addItems(lng);

    QStringList doc;
    doc << "Choisissez un type de document"
          << "Livre"
          << "Chapitre de livre"
          << "Article d'une revue"
          << "Article d'un journal"
          << "Dissertation"
          << "Site Web"
         ;
    listOfDocumentsBox->addItems(doc);

    QStringList dis;
    dis << "Dissertation Type"
        << "Licence dissertation"
        << "Master dissertation"
        << "Magister dissertation"
        << "Phd thesis"
           ;
    dissertationTypeBox->addItems(dis);

    QStringList formatType;
    formatType << "format type"
               << "Italique (recommandé)"
               << "Gras"
               << "sougligné"
                  ;
    listOfFormatsBox->addItems(formatType);

    // addNames Buttons
    addAuthorsButton = new QPushButton(this);
    addAuthorsButton->setIcon(QIcon(":images/add.png"));
    addEditorsButton = new QPushButton(this);
    addEditorsButton->setIcon(QIcon(":images/add.png"));
    addTranslatorButton = new QPushButton(this);
    addTranslatorButton->setIcon(QIcon(":images/add.png"));

    deleteAuthorButton = new QPushButton(this);
    deleteAuthorButton->setIcon(QIcon(":images/delete.png"));
    deleteEditorButton = new QPushButton(this);
    deleteEditorButton->setIcon(QIcon(":images/delete.png"));
    deleteTranslatorButton = new QPushButton(this);
    deleteTranslatorButton->setIcon(QIcon(":images/delete.png"));

    title = new Field(this);
    institution = new Field(this);
    chapter = new Field(this);
    journal = new Field(this);
    issueNumberForJournal = new Field(this);
    volumNumber = new Field(this);
    volumTitle = new Field(this);
    editionNumber = new Field(this);
    countryOfPublication = new Field(this);
    editionsHome = new Field(this);
    dateOfPublication = new DateField(this); ///////
    currentPage = new Field(this);
    pages = new PagesField(this);
    webSiteName = new Field(this);
    webSiteUrl = new Field(this);
    accessDate = new DateField(this);
    DOI = new Field(this);
    ISBN = new Field(this);

    bibliographyDisplayerLabel = new QLabel(this);
    citationDisplayerLabel = new QLabel(this);

    citationDisplayer = new QTextEdit(this);
    citationDisplayer->setReadOnly(true);
    bibliographyDisplayer = new QTextEdit(this);
    bibliographyDisplayer->setReadOnly(true);

    int minH = 40;
    int maxH = 130;
    citationDisplayer->setMaximumHeight(maxH);
    citationDisplayer->setMinimumHeight(minH);
    bibliographyDisplayer->setMaximumHeight(maxH);
    bibliographyDisplayer->setMinimumHeight(minH);

    copyCitationButton = new QPushButton;
    copyBibliographyButton = new QPushButton;
}

void CitatoGui::setupStyle(){

//     round shape of the buttons
    QString buttonsShape = "border-style: none;"
                            "border-width:0px;"
                            "border-radius:20px;"
                            "max-width:21px;"
                            "max-height:21px;"
                            "min-width:20px;"
                            "min-height:20px;";

    addAuthorsButton->setStyleSheet(buttonsShape);
    addEditorsButton->setStyleSheet(buttonsShape);
    addTranslatorButton->setStyleSheet(buttonsShape);

    deleteAuthorButton->setStyleSheet(buttonsShape);
    deleteEditorButton->setStyleSheet(buttonsShape);
    deleteTranslatorButton->setStyleSheet(buttonsShape);

    // fonts
    QFontDatabase::addApplicationFont(":/fonts/LiberationSerif-Regular.ttf");
    QFontDatabase::addApplicationFont(":/fonts/simplified arabic.ttf");
}

void CitatoGui::setupLayout(){

    // header/first row layouts
    headerLayout = new QHBoxLayout;
    headerLayout->addWidget(generateButton);
    headerLayout->addWidget(listOfDocumentsBox);
    headerLayout->addWidget(listOfLanguagesBox);
    headerLayout->addWidget(listOfFormatsBox);
    headerLayout->addWidget(clearAllButton);

    // heach label and its lineEdit will have a horizontal layout
    authorsWidgetsLayout = new QVBoxLayout;
    editorsWidgetsLayout = new QVBoxLayout;
    translatorsWidgetsLayout = new QVBoxLayout;


    scrollAreaLayout = new QVBoxLayout;		// the layout used by the widget inside the scrole area
    widgetOfScrollArea->setLayout(scrollAreaLayout);

    scrollAreaLayout->addLayout(authorsWidgetsLayout);
    scrollAreaLayout->addWidget(title);
        title->getLayout()->addWidget(dissertationTypeBox);
    scrollAreaLayout->addWidget(volumNumber);
    scrollAreaLayout->addWidget(volumTitle);
    scrollAreaLayout->addWidget(institution);
    scrollAreaLayout->addWidget(chapter);
    scrollAreaLayout->addWidget(journal);
    scrollAreaLayout->addWidget(issueNumberForJournal);
    scrollAreaLayout->addLayout(editorsWidgetsLayout);
    scrollAreaLayout->addLayout(translatorsWidgetsLayout);
    scrollAreaLayout->addWidget(editionNumber);
    scrollAreaLayout->addWidget(countryOfPublication);
    scrollAreaLayout->addWidget(editionsHome);
    scrollAreaLayout->addWidget(dateOfPublication);	//////////
    scrollAreaLayout->addWidget(currentPage);
    scrollAreaLayout->addWidget(pages);
    scrollAreaLayout->addWidget(webSiteName);
    scrollAreaLayout->addWidget(webSiteUrl);
    scrollAreaLayout->addWidget(accessDate);
    scrollAreaLayout->addWidget(DOI);
    scrollAreaLayout->addWidget(ISBN);


    QVBoxLayout *displayerLayout = new QVBoxLayout;
    displayerLayout->addWidget(citationDisplayerLabel);
    displayerLayout->addWidget(citationDisplayer);
    displayerLayout->addWidget(bibliographyDisplayerLabel);
    displayerLayout->addWidget(bibliographyDisplayer);

    // buttons that copies the result
    QHBoxLayout *copyButtons = new QHBoxLayout;
    copyButtons->addWidget(copyCitationButton);
    copyButtons->addWidget(copyBibliographyButton);

    // place everything in the main layout
    mainLayout->addLayout(headerLayout, 0, 0);
    mainLayout->addWidget(fieldsContainerScrollArea, 1, 0);
    mainLayout->addLayout(displayerLayout, 2, 0);
    mainLayout->addLayout(copyButtons, 3, 0);
}


void CitatoGui::setupMenu(){
    // construct menu and statu bar
    menuFile = new QMenu;
    menuFile = menuBar()->addMenu("File");

//    openCitatoManagerAction = new QAction("Open Citato Manager", this);
//    menuFile->addAction(openCitatoManagerAction);

    openCitatoFileAction = new QAction("Open", this);
    menuFile->addAction(openCitatoFileAction);

    saveCitatoFileAction = new QAction("Save", this);
    menuFile->addAction(saveCitatoFileAction);

    quitAction = new QAction("Quit", this);
    quitAction->setIcon(QIcon(":/images/exit.png"));
    menuFile->addSeparator();
    menuFile->addAction(quitAction);
    menuFile->addSeparator();

    optionsMenu = new QMenu;
    optionsMenu = menuBar()->addMenu("Options");

    ChooseStyleAction = new QMenu("Choose a Citation Style", this);
    optionsMenu->addMenu(ChooseStyleAction);

    listOfCitationStylesActionGroup = new QActionGroup(this);

    listOfCitationStylesActionGroup->setExclusive(true);
    chicagoFootnoteStyleAction = new QAction("Chicago Style (footnote)");
    chicagoInTextStyleAction = new QAction("Chicago Style (in-text)");

    chicagoFootnoteStyleAction->setCheckable(true);
    chicagoInTextStyleAction->setCheckable(true);

    listOfCitationStylesActionGroup->addAction(chicagoFootnoteStyleAction);
    listOfCitationStylesActionGroup->addAction(chicagoInTextStyleAction);

    ChooseStyleAction->addAction(chicagoFootnoteStyleAction);
    ChooseStyleAction->addAction(chicagoInTextStyleAction);


    chooseCitatoTheme = new QMenu("Choose a theme", this);
    optionsMenu->addMenu(chooseCitatoTheme);

    listOfthemes = new QActionGroup(this);
    listOfthemes->setExclusive(true);
    nativeThemeAction = new QAction("Native");
    nativeThemeAction->setCheckable(true);
    darkBlueThemeAction = new QAction("Dark Theme (blue borders)");
    darkBlueThemeAction->setCheckable(true);
    lightBlueThemeAction = new QAction("Light Theme (blue borders)");
    lightBlueThemeAction->setCheckable(true);
    darkOrangeThemeAction = new QAction("Dark Theme (orange borders)");
    darkOrangeThemeAction->setCheckable(true);
    lightOrangeThemeAction = new QAction("Light Theme (orange borders)");
    lightOrangeThemeAction->setCheckable(true);
    darkGreenThemeAction = new QAction("Dark Theme (green borders)");
    darkGreenThemeAction->setCheckable(true);
    lightGreenThemeAction = new QAction("Light Theme (green borders)");
    lightGreenThemeAction->setCheckable(true);

    listOfthemes->addAction(nativeThemeAction);
    listOfthemes->addAction(darkBlueThemeAction);
    listOfthemes->addAction(lightBlueThemeAction);
    listOfthemes->addAction(darkOrangeThemeAction);
    listOfthemes->addAction(lightOrangeThemeAction);
    listOfthemes->addAction(darkGreenThemeAction);
    listOfthemes->addAction(lightGreenThemeAction);

    chooseCitatoTheme->addAction(nativeThemeAction);
    chooseCitatoTheme->addAction(darkBlueThemeAction);
    chooseCitatoTheme->addAction(lightBlueThemeAction);
    chooseCitatoTheme->addAction(darkOrangeThemeAction);
    chooseCitatoTheme->addAction(lightOrangeThemeAction);
    chooseCitatoTheme->addAction(darkGreenThemeAction);
    chooseCitatoTheme->addAction(lightGreenThemeAction);


    interogationMenu = new QMenu;
    interogationMenu = menuBar()->addMenu("?");

    reportBugAction = new QAction("Report a Bug", this);
    aboutCitatoAction = new QAction("About Citato", this);
    aboutCitatoAction->setIcon(QIcon(":/images/citato.png"));
    updateCitatoAction = new QAction("Check for Updates", this);
    interogationMenu->addAction(updateCitatoAction);
    interogationMenu->addAction(reportBugAction);
    interogationMenu->addAction(aboutCitatoAction);

    // status bar
    statusB = new QStatusBar;
    statusB = statusBar();
    creatorSZ = new QLabel;
    creatorSZ->setText("Creator: Zineddine Saïbi");
    creatorSZ->setAlignment(Qt::AlignRight);
    statusB->addWidget(creatorSZ, 1);
}


void CitatoGui::setupSignalsSlots()
{

    connect(clearAllButton, SIGNAL(clicked(bool)), this, SLOT(clearAllWidgets()));
    connect(generateButton, SIGNAL(clicked(bool)), this, SLOT(callComposer()));
    connect(listOfLanguagesBox, SIGNAL(currentIndexChanged(int)), this, SLOT(changeLanguage()));
    connect(listOfDocumentsBox, SIGNAL(currentIndexChanged(int)), this, SLOT(changeDocument()));
    connect(addAuthorsButton, SIGNAL(clicked(bool)), this, SLOT(addAuthorField()));
    connect(deleteAuthorButton, SIGNAL(clicked(bool)), this, SLOT(deleteAuthorField()));
    connect(addEditorsButton, SIGNAL(clicked(bool)), this, SLOT(addEditorField()));
    connect(deleteEditorButton, SIGNAL(clicked(bool)), this, SLOT(deleteEditorField()));
    connect(addTranslatorButton, SIGNAL(clicked(bool)), this, SLOT(addTranslatorField()));
    connect(deleteTranslatorButton, SIGNAL(clicked(bool)), this, SLOT(deleteTranslatorField()));
    connect(copyCitationButton, SIGNAL(clicked(bool)), this, SLOT(copyCitation()));
    connect(copyBibliographyButton, SIGNAL(clicked(bool)), this, SLOT(copyBibliography()));
//    connect(openCitatoManagerAction, SIGNAL(triggered(bool)), this, SLOT(openCitatoManager()));
    connect(saveCitatoFileAction, SIGNAL(triggered(bool)), this, SLOT(saveDocument()));
    connect(openCitatoFileAction, SIGNAL(triggered(bool)), this, SLOT(loadDocument()));
    connect(quitAction, SIGNAL(triggered(bool)), this, SLOT(close()));
    connect(reportBugAction, SIGNAL(triggered(bool)), this, SLOT(reportBug()));
    connect(aboutCitatoAction, SIGNAL(triggered(bool)), this, SLOT(aboutCitato()));
    connect(updateCitatoAction, SIGNAL(triggered(bool)), this, SLOT(updateCitato()));
    connect(listOfthemes, SIGNAL(triggered(QAction*)), this, SLOT(changeTheme()));
}

void CitatoGui::setupKeyboardShortcuts(){
    quitAction->setShortcut(Qt::Key_Q | Qt::CTRL);
    generateButton->setShortcut(Qt::Key_G | Qt::CTRL);
    generateButton->setShortcut(Qt::Key_Enter);
    clearAllButton->setShortcut(Qt::Key_E | Qt::CTRL);
    saveCitatoFileAction->setShortcut(Qt::Key_S | Qt::CTRL);
    openCitatoFileAction->setShortcut(Qt::Key_O | Qt::CTRL);
}

void CitatoGui::saveDocument()
{
    constructElement();
    CitatoFile *cf = new CitatoFile(element, this);
    if(cf->isValidWithWarning()) { cf->saveToFile(); }
    freeElement();
    delete cf;
}


void CitatoGui::displayDocument(DocumentInfo &info)
{
    clearAllWidgets();		// clear all user input. otherwise, there would be indesirable effects.

    // ComboBoxes related to language, document type and dissertation type
    dissertationTypeBox->setCurrentIndex(info.getDissertationTypeInt());
    listOfDocumentsBox->setCurrentIndex(info.getDocumentTypeInt());
    listOfLanguagesBox->setCurrentIndex(info.getDocumentLanguageInt());

    // list of authors, editors and translators
    int i = 0;
    while( i < info.getAuthorsList().size() ) {
        authorsNameWidgetList.at(i)->setName( info.getAuthorsList().at(i) );
        ++i;
        if(i != info.getAuthorsList().size()) { addAuthorField(); }
        else{ break; }
    }

    i = 0;
    while( i < info.getEditorsList().size() ) {
        editorsNameWidgetList.at(i)->setName( info.getEditorsList().at(i) );
        ++i;
        if(i != info.getEditorsList().size()) { addEditorField(); }
        else{ break; }
    }

    i = 0;
    while( i < info.getTranslatorsList().size() ) {
        translatorsNameWidgetList.at(i)->setName( info.getTranslatorsList().at(i) );
        ++i;
        if(i != info.getTranslatorsList().size()) { addTranslatorField(); }
        else{ break; }
    }

    // the other fields
    title->setLineEditText(info.getTitle());
    institution->setLineEditText(info.getInstitution());
    chapter->setLineEditText(info.getChapter());
    journal->setLineEditText(info.getJournal());
    issueNumberForJournal->setLineEditText(info.getIssueNumberJournal());
    editionNumber->setLineEditText(info.getEditionNumber());
    editionsHome->setLineEditText(info.getEditionsHome());
    volumNumber->setLineEditText(info.getVolumNumber());
    volumTitle->setLineEditText(info.getVolumTitle());
    countryOfPublication->setLineEditText(info.getLocation());
    dateOfPublication->setLineEditText(info.getDayOfPublication(),
                                       info.getMonthOfPublication(),
                                       info.getYearOfPublication());
    currentPage->setLineEditText(info.getCurrentPage());
    pages->setLineEditText(info.getPageFrom(), info.getPageTo());
    webSiteName->setLineEditText(info.getWebSiteName());
    webSiteUrl->setLineEditText(info.getWebSiteUrl());
    accessDate->setLineEditText(info.getAccessDay(),
                                info.getAccessMonth(),
                                info.getAccessYear());
    DOI->setLineEditText(info.getDOI());
    ISBN->setLineEditText(info.getISBN());
}

// receives a jsonDocument and import it to citato to display it
void CitatoGui::displayDocument(QJsonDocument *js)
{
    CitatoFile *cf = new CitatoFile(js, this);
    displayDocument(*(cf->getDocumentInfo()));
    delete cf;
}

void CitatoGui::loadDocument()
{
    QJsonDocument *js = new QJsonDocument(CitatoFile::loadFromFile());
    if(js->isEmpty()) { return; }
    displayDocument(js);

    delete js;
}


void CitatoGui::openCitatoManager(){
    citatoManager = new CitatoManagerGui(this);
    citatoManager->show();
}

void CitatoGui::dragEnterEvent(QDragEnterEvent *event){
    if(event->mimeData()->hasUrls()){
        event->acceptProposedAction();
    }
}


void CitatoGui::dropEvent(QDropEvent *event){
    if(event->mimeData()->urls().size() > 1) {
        QMessageBox::warning(this, "Error", "Please choose only one file to drop in Citato.");
        return;
    }

    QList<QUrl> urls = event->mimeData()->urls();

    for(int i = 0 ; i < urls.size() ; ++i){
        QFile *f = new QFile(urls.at(i).toLocalFile());

        if(!f->open(QIODevice::ReadOnly)){
            QMessageBox::critical(this, "Not valid file", "This file cannot be opened");
            return;
        }

        QJsonDocument *js = new QJsonDocument(CitatoFile::fromFileToJsonDocument(f));
        if(f->isOpen()) { f->close(); }
        if(!CitatoFile::isValidWithWarning(js)) { return; }

        displayDocument(js);

        delete f;
        delete js;
    }
}


void CitatoGui::changeLanguage(){
    // change the text of labels and place holders following the chosed language

    if(listOfLanguagesBox->currentIndex() < 2)		// English or none
    {
        // Alignment and font
        fieldsContainerScrollArea->setLayoutDirection(Qt::LeftToRight);
        citationDisplayer->setLayoutDirection(Qt::LeftToRight);
        citationDisplayer->setFont(QFont(FONT_ROMAN, 13, QFont::Normal));
        bibliographyDisplayer->setLayoutDirection(Qt::LeftToRight);
        bibliographyDisplayer->setFont(QFont(FONT_ROMAN, 13, QFont::Normal));

        for(int i = 0 ; i < authorsNameWidgetList.size() ; ++i){
            authorsNameWidgetList.at(i)->adaptLanguage(listOfLanguagesBox->currentIndex());
        }
        for(int i = 0 ; i < editorsNameWidgetList.size() ; ++i){
            editorsNameWidgetList.at(i)->adaptLanguage(listOfLanguagesBox->currentIndex());
        }
        for(int i = 0 ; i < translatorsNameWidgetList.size() ; ++i){
            translatorsNameWidgetList.at(i)->adaptLanguage(listOfLanguagesBox->currentIndex());
        }

        // Buttons
        copyCitationButton->setText("Copy citation");
        copyBibliographyButton->setText("Copy bibliography");

        // Document type ComboBox
        listOfDocumentsBox->setLayoutDirection(Qt::LeftToRight);
        listOfDocumentsBox->setItemText(0, "Choose a document type");
        listOfDocumentsBox->setItemText(1, "Book");
        listOfDocumentsBox->setItemText(2, "Chapter's Book");
        listOfDocumentsBox->setItemText(3, "Scientific Article");
        listOfDocumentsBox->setItemText(4, "Newspaper Article");
        listOfDocumentsBox->setItemText(5, "Dissertation");
        listOfDocumentsBox->setItemText(6, "Web Site");

        // dissertation type ComboBox
        dissertationTypeBox->setLayoutDirection(Qt::LeftToRight);
        dissertationTypeBox->setItemText(0, "Dissertation Type");
        dissertationTypeBox->setItemText(1, "Licence dissertation");
        dissertationTypeBox->setItemText(2, "Master dissertation");
        dissertationTypeBox->setItemText(3, "Magister dissertation");
        dissertationTypeBox->setItemText(4, "Phd thesis");


        // change the text of labels and placeholders of lineEdits
        title->changeText("Title", "Title of the book or article");
        institution->changeText("Institution", "University | Government Institution | International Organisation");
        chapter->changeText("Chapter's title", "The title of the chapter");
        journal->changeText("Journal");
        issueNumberForJournal->changeText("Issue Number", "journal's issue number");
        volumNumber->changeText("Volum Number");
        volumTitle->changeText("Volum Title");
        editionNumber->changeText("Edition Number");
        countryOfPublication->changeText("Country/City of Publication", "ex: Algeria");
        editionsHome->changeText("Editions Home", "ex: l'Harmattan");
        dateOfPublication->changeText("Date of Publication", "year", "month or season", "day");
        currentPage->changeText("Current Page");
        pages->changeText("Document Pages", "from", "to");
        webSiteName->changeText("Web Site Name", "ex: Wikipedia");
        webSiteUrl->changeText("Web Site URL", "https://");
        accessDate->changeText("Access Date", "year", "month or season", "day");
        DOI->changeText("DOI", "ex: 10.2307/2181906");
        ISBN->changeText("ISBN Number");

        citationDisplayerLabel->setText("Citation");
        bibliographyDisplayerLabel->setText("Bibliography Entry");

        // tooltip
        title->setToolTip("");
        institution->setToolTip("official institution | think tank | international organization | university");
        chapter->setToolTip("title of the chapter from a book");
        journal->setToolTip("name of the periodiacal");
        issueNumberForJournal->setToolTip("");
        volumNumber->setToolTip("");
        volumTitle->setToolTip("");
        editionNumber->setToolTip("");
        countryOfPublication->setToolTip("ex: Algeria, Algiers");
        editionsHome->setToolTip("");
        dateOfPublication->setToolTip("");
        currentPage->setToolTip("");
        pages->setToolTip("");
        webSiteName->setToolTip("");
        webSiteUrl->setToolTip("");
        accessDate->setToolTip("");
        DOI->setToolTip("");
        ISBN->setToolTip("");
    }

    else if(listOfLanguagesBox->currentIndex() == 2)		// French
    {
        // Alignment and font
        fieldsContainerScrollArea->setLayoutDirection(Qt::LeftToRight);
        citationDisplayer->setLayoutDirection(Qt::LeftToRight);
        citationDisplayer->setFont(QFont(FONT_ROMAN, 13, QFont::Normal));
        bibliographyDisplayer->setLayoutDirection(Qt::LeftToRight);
        bibliographyDisplayer->setFont(QFont(FONT_ROMAN, 13, QFont::Normal));

        for(int i = 0 ; i < authorsNameWidgetList.size() ; ++i){
            authorsNameWidgetList.at(i)->adaptLanguage(listOfLanguagesBox->currentIndex());
        }
        for(int i = 0 ; i < editorsNameWidgetList.size() ; ++i){
            editorsNameWidgetList.at(i)->adaptLanguage(listOfLanguagesBox->currentIndex());
        }
        for(int i = 0 ; i < translatorsNameWidgetList.size() ; ++i){
            translatorsNameWidgetList.at(i)->adaptLanguage(listOfLanguagesBox->currentIndex());
        }

        // Buttons
        copyCitationButton->setText("Copier la citation");
        copyBibliographyButton->setText("Copier l'entrée bibliographique");

        // Document type ComboBox
        listOfDocumentsBox->setLayoutDirection(Qt::LeftToRight);
        listOfDocumentsBox->setItemText(0, "Choisissez un Type de Document");
        listOfDocumentsBox->setItemText(1, "Livre");
        listOfDocumentsBox->setItemText(2, "Chapitre de Livre");
        listOfDocumentsBox->setItemText(3, "Article d'une revue scientifique");
        listOfDocumentsBox->setItemText(4, "Article d'un journal");
        listOfDocumentsBox->setItemText(5, "Mémoire/Thèse");
        listOfDocumentsBox->setItemText(6, "Site web");

        // dissertation type ComboBox
        dissertationTypeBox->setLayoutDirection(Qt::LeftToRight);
        dissertationTypeBox->setItemText(0, "Type de Dissertation");
        dissertationTypeBox->setItemText(1, "Mémoire de Licence");
        dissertationTypeBox->setItemText(2, "Mémoire de Master");
        dissertationTypeBox->setItemText(3, "Mémoire de Magistère");
        dissertationTypeBox->setItemText(4, "Thèse de Doctorat");

        // labels

        title->changeText("Titre du livre/article");
        institution->changeText("Institution", "Université | Institution gouvernementale | Organisation internationale");
        chapter->changeText("Titre du chapitre", "titre du chapitre");
        journal->changeText("Revue");
        issueNumberForJournal->changeText("Numéro du journal");
        volumNumber->changeText("Numéro du Volume");
        volumTitle->changeText("Title du Volume");
        editionNumber->changeText("Numéro d'Édition");
        countryOfPublication->changeText("Pays/ville de Publication");
        editionsHome->changeText("Maison d'édition");
        dateOfPublication->changeText("Date de Publication", "Année", "mois ou saison", "jour");
        currentPage->changeText("Page courante");
        pages->changeText("Pages du Document", "de", "à");
        webSiteName->changeText("Nom du Site Web", "ex: wikipedia");
        webSiteUrl->changeText("URL du Site Web", "https://");
        accessDate->changeText("Date d'accès", "Année", "mois ou saison", "jour");
        DOI->changeText("DOI", "ex: 10.2307/2181906");
        ISBN->changeText("ISBN");


        citationDisplayerLabel->setText("Citation");
        bibliographyDisplayerLabel->setText("Entrée bibliographique");

        // tooltip
        title->setToolTip("");
        institution->setToolTip("");
        chapter->setToolTip("");
        journal->setToolTip("");
        issueNumberForJournal->setToolTip("");
        volumNumber->setToolTip("");
        volumTitle->setToolTip("");
        editionNumber->setToolTip("");
        countryOfPublication->setToolTip("");
        editionsHome->setToolTip("");
        dateOfPublication->setToolTip("");
        currentPage->setToolTip("");
        pages->setToolTip("");
        webSiteName->setToolTip("");
        webSiteUrl->setToolTip("");
        accessDate->setToolTip("");
        DOI->setToolTip("");
        ISBN->setToolTip("");
    }

    else if(listOfLanguagesBox->currentIndex() == 3)		// Arabic
    {
        // Alignment and font
        fieldsContainerScrollArea->setLayoutDirection(Qt::RightToLeft);
        citationDisplayer->setLayoutDirection(Qt::LeftToRight);
        citationDisplayer->setFont(QFont(FONT_ARABIC, 13, QFont::Normal));
        bibliographyDisplayer->setLayoutDirection(Qt::LeftToRight);
        bibliographyDisplayer->setFont(QFont(FONT_ARABIC, 13, QFont::Normal));

        // place holders of names fields
        for(int i = 0 ; i < authorsNameWidgetList.size() ; ++i){
            authorsNameWidgetList.at(i)->adaptLanguage(listOfLanguagesBox->currentIndex());
        }
        for(int i = 0 ; i < editorsNameWidgetList.size() ; ++i){
            editorsNameWidgetList.at(i)->adaptLanguage(listOfLanguagesBox->currentIndex());
        }
        for(int i = 0 ; i < translatorsNameWidgetList.size() ; ++i){
            translatorsNameWidgetList.at(i)->adaptLanguage(listOfLanguagesBox->currentIndex());
        }

        // Buttons
        copyCitationButton->setText("نسخ المرجع");
        copyBibliographyButton->setText("نسخ البيبليوغراقيا");

        // Document type ComboBox
        listOfDocumentsBox->setLayoutDirection(Qt::RightToLeft);
        listOfDocumentsBox->setItemText(0, "اختر نوع الوثيقة");
        listOfDocumentsBox->setItemText(1, "كتاب");
        listOfDocumentsBox->setItemText(2, "فصل من كتاب");
        listOfDocumentsBox->setItemText(3, "مقال من مجلة علمية");
        listOfDocumentsBox->setItemText(4, "مقال من جريدة");
        listOfDocumentsBox->setItemText(5, "مدكرة أكاديمية");
        listOfDocumentsBox->setItemText(6, "موقع انترنت");

        // dissertation type ComboBox
        dissertationTypeBox->setLayoutDirection(Qt::RightToLeft);
        dissertationTypeBox->setItemText(0, "نوع المذكرة");
        dissertationTypeBox->setItemText(1, "مذكرة ليسانس");
        dissertationTypeBox->setItemText(2, "مذكرة ماستر");
        dissertationTypeBox->setItemText(3, "رسالة ماجستير");
        dissertationTypeBox->setItemText(4, "أطروحة دكتوراه");

        // labels
        title->changeText("العنوان");
        institution->changeText("المؤسسة",
                                "جامعة | مأسسة حكومية | منظمة دولية");
        chapter->changeText("عنوان الفصل",
                            "عنوان الفصل");
        journal->changeText("المجلة",
                            "عنوان الدورية، المجلة...");
        issueNumberForJournal->changeText("العدد");
        volumNumber->changeText("رقم المجلد");
        volumTitle->changeText("عنوان المجلد");
        editionNumber->changeText("رقم الطبعة");
        countryOfPublication->changeText("بلد النشر",
                                         "أمثلة:  الجزائر | باريس، فرنسا");
        editionsHome->changeText("دار النشر");
        dateOfPublication->changeText("تاريخ النشر", "السنة",
                                      "الشهر", "البوم");
        currentPage->changeText("الصفحة الحالية");
        pages->changeText("صفحات الوثيقة",
                          "من",
                          "إلى");
        webSiteName->changeText("إسم موقع الإنترنيت",
                                "مثل: ويكيبيديا");
        webSiteUrl->changeText("موقع الإنترنيت", "https://");
        accessDate->changeText("تاريخ التصفح",
                               "السنة",
                               "الشهر",
                               "البوم");

        QString aTemp = "10.2307/2181906";
        QString bTemp = "مثال: ";
        DOI->changeText("معرف الوثيقة الرقمي",
                        bTemp + aTemp);
        ISBN->changeText("الرقم الدولي المعياري للكتاب");

        citationDisplayerLabel->setText("الهامش");
        bibliographyDisplayerLabel->setText("قائمة المراجع");


        // tooltip
        title->setToolTip("");
        institution->setToolTip("");
        chapter->setToolTip("");
        journal->setToolTip("");
        issueNumberForJournal->setToolTip("");
        volumNumber->setToolTip("");
        volumTitle->setToolTip("");
        editionNumber->setToolTip("");
        countryOfPublication->setToolTip("");
        editionsHome->setToolTip("");
        dateOfPublication->setToolTip("");
        currentPage->setToolTip("");
        pages->setToolTip("");
        webSiteName->setToolTip("");
        webSiteUrl->setToolTip("");
        accessDate->setToolTip("");
        DOI->setToolTip("");
        ISBN->setToolTip("");

    }
}


void CitatoGui::changeTheme()
{
    QSettings settings("Citato_experimental", "citato_main_experimental");
    settings.beginGroup("main");	// save the chosen theme to load it the next time

    if(nativeThemeAction->isChecked()){
        QFile f(":qdarkstyle/style_native.qss");
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        qApp->setStyleSheet(ts.readAll());

        settings.setValue("Theme", QVariant("native"));
    }
    else if(darkBlueThemeAction->isChecked()){
        QFile f(":qdarkstyle/style_dark_blue.qss");
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        qApp->setStyleSheet(ts.readAll());
        settings.setValue("Theme", QVariant("style_dark_blue"));
    }
    else if(lightBlueThemeAction->isChecked()){
        QFile f(":qdarkstyle/style_light_blue.qss");
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        qApp->setStyleSheet(ts.readAll());
        settings.setValue("Theme", QVariant("style_light_blue"));
    }
    else if(darkOrangeThemeAction->isChecked()){
        QFile f(":qdarkstyle/style_dark_orange.qss");
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        qApp->setStyleSheet(ts.readAll());
        settings.setValue("Theme", QVariant("style_dark_orange"));

    }
    else if(lightOrangeThemeAction->isChecked()){
        QFile f(":qdarkstyle/style_light_orange.qss");
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        qApp->setStyleSheet(ts.readAll());
        settings.setValue("Theme", QVariant("style_light_orange"));
    }
    else if(darkGreenThemeAction->isChecked()){
        QFile f(":qdarkstyle/style_dark_green.qss");
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        qApp->setStyleSheet(ts.readAll());
        settings.setValue("Theme", QVariant("style_dark_green"));
    }
    else if(lightGreenThemeAction->isChecked()){
        QFile f(":qdarkstyle/style_light_green.qss");
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        qApp->setStyleSheet(ts.readAll());
        settings.setValue("Theme", QVariant("style_light_green"));
    }
    else{
        QFile f(":qdarkstyle/style_native.qss");
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        qApp->setStyleSheet(ts.readAll());
        settings.setValue("Theme", QVariant("native"));
    }

    settings.endGroup();
}


void CitatoGui::clearAllWidgets()
{
    // clear text of user input

    // remove all name fields except the first one
    while(true){
        if(authorsNameWidgetList.size() == 1) { break; }
        delete authorsNameWidgetList.last();
        authorsNameWidgetList.removeLast();
    }
    while(true){
        if(editorsNameWidgetList.size() == 1) { break; }
        delete editorsNameWidgetList.last();
        editorsNameWidgetList.removeLast();
    }
    while(true){
        if(translatorsNameWidgetList.size() == 1) { break; }
        delete translatorsNameWidgetList.last();
        translatorsNameWidgetList.removeLast();
    }

    // clear the text of every field
    authorsNameWidgetList.at(0)->clear();
    title->clear();
    dissertationTypeBox->setCurrentIndex(0);
    institution->clear();
    chapter->clear();
    journal->clear();
    issueNumberForJournal->clear();
    editorsNameWidgetList.at(0)->clear();
    translatorsNameWidgetList.at(0)->clear();
    volumNumber->clear();
    volumTitle->clear();
    editionNumber->clear();
    countryOfPublication->clear();
    editionsHome->clear();
    dateOfPublication->clear();
    currentPage->clear();
    pages->clear();
    webSiteName->clear();
    webSiteUrl->clear();
    accessDate->clear();
    DOI->clear();
    ISBN->clear();

    citationDisplayer->clear();
    bibliographyDisplayer->clear();

    if(composer){
        delete composer;
    }

    freeElement();

    citationString.clear();
    bibliographyString.clear();
}


void CitatoGui::changeDocument(){
    // following the selected document, fields and their labels have to be
    // disabled and made invisible, other have to be enabled

    if(listOfDocumentsBox->currentIndex() < 2){		// book or none

        for(auto i = authorsNameWidgetList.begin() ; i != authorsNameWidgetList.end() ; i++){
            enable(*i, true);
        }
        for(auto i = editorsNameWidgetList.begin() ; i != editorsNameWidgetList.end() ; i++){
            enable(*i, true);
        }
        for(auto i = translatorsNameWidgetList.begin() ; i != translatorsNameWidgetList.end() ; i++){
            enable(*i, true);
        }

        enable(title, true);
            enable(dissertationTypeBox, false);
        enable(institution, false);
        enable(chapter, false);
        enable(journal, false);
        enable(issueNumberForJournal, false);
        enable(volumNumber, true);
        enable(volumTitle, true);
        enable(editionNumber, true);
        enable(countryOfPublication, true);
        enable(editionsHome, true);
        enable(dateOfPublication, true);
        enable(currentPage, true);
        enable(pages, false);
        enable(webSiteName, false);
        enable(webSiteUrl, true);
        enable(accessDate, false);
        enable(DOI, false);
        enable(ISBN, false);
    }

    else if(listOfDocumentsBox->currentIndex() == 2){		// chapters

        for(auto i = authorsNameWidgetList.begin() ; i != authorsNameWidgetList.end() ; i++){
            enable(*i, true);
        }
        for(auto i = editorsNameWidgetList.begin() ; i != editorsNameWidgetList.end() ; i++){
            enable(*i, true);
        }
        for(auto i = translatorsNameWidgetList.begin() ; i != translatorsNameWidgetList.end() ; i++){
            enable(*i, true);
        }

        enable(title, true);
            enable(dissertationTypeBox, false);
        enable(institution, false);
        enable(chapter, true);
        enable(journal, false);
        enable(issueNumberForJournal, false);
        enable(volumNumber, true);
        enable(volumTitle, true);
        enable(editionNumber, true);
        enable(countryOfPublication, true);
        enable(editionsHome, true);
        enable(dateOfPublication, true);
        enable(currentPage, true);
        enable(pages, true);
        enable(webSiteName, false);
        enable(webSiteUrl, true);
        enable(accessDate, false);
        enable(DOI, false);
        enable(ISBN, false);
    }

    else if(listOfDocumentsBox->currentIndex() == 3){		// scientific articles

        for(auto i = authorsNameWidgetList.begin() ; i != authorsNameWidgetList.end() ; i++){
            enable(*i, true);
        }
        for(auto i = editorsNameWidgetList.begin() ; i != editorsNameWidgetList.end() ; i++){
            enable(*i, false);
        }
        for(auto i = translatorsNameWidgetList.begin() ; i != translatorsNameWidgetList.end() ; i++){
            enable(*i, false);
        }


        enable(title, true);
            enable(dissertationTypeBox, false);
        enable(institution, false);
        enable(chapter, false);
        enable(journal, true);
        enable(issueNumberForJournal, true);
        enable(volumNumber, true);
        enable(volumTitle, false);
        enable(editionNumber, false);
        enable(countryOfPublication, false);
        enable(editionsHome, false);
        enable(dateOfPublication, true);
        enable(currentPage, true);
        enable(pages, true);
        enable(webSiteName, false);
        enable(webSiteUrl, true);
        enable(accessDate, false);
        enable(DOI, true);
        enable(ISBN, false);
    }

    else if(listOfDocumentsBox->currentIndex() == 4){	// newspaper

        for(auto i = authorsNameWidgetList.begin() ; i != authorsNameWidgetList.end() ; i++){
            enable(*i, true);
        }
        for(auto i = editorsNameWidgetList.begin() ; i != editorsNameWidgetList.end() ; i++){
            enable(*i, false);
        }
        for(auto i = translatorsNameWidgetList.begin() ; i != translatorsNameWidgetList.end() ; i++){
            enable(*i, false);
        }

        enable(title, true);
            enable(dissertationTypeBox, false);
        enable(institution, false);
        enable(chapter, false);
        enable(journal, true);
        enable(issueNumberForJournal, false);
        enable(volumNumber, false);
        enable(volumTitle, false);
        enable(editionNumber, false);
        enable(countryOfPublication, false);
        enable(editionsHome, false);
        enable(dateOfPublication, true);
        enable(currentPage, false);
        enable(pages, false);
        enable(webSiteName, false);
        enable(webSiteUrl, true);
        enable(accessDate, false);
        enable(DOI, false);
        enable(ISBN, false);
    }

    else if(listOfDocumentsBox->currentIndex() == 5){	// dissertation

        for(auto i = authorsNameWidgetList.begin() ; i != authorsNameWidgetList.end() ; i++){
            enable(*i, true);
        }
        for(auto i = editorsNameWidgetList.begin() ; i != editorsNameWidgetList.end() ; i++){
            enable(*i, false);
        }
        for(auto i = translatorsNameWidgetList.begin() ; i != translatorsNameWidgetList.end() ; i++){
            enable(*i, false);
        }

        enable(title, true);
            enable(dissertationTypeBox, true);
        enable(institution, true);
        enable(chapter, false);
        enable(journal, false);
        enable(issueNumberForJournal, false);
        enable(volumNumber, false);
        enable(volumTitle, false);
        enable(editionNumber, false);
        enable(countryOfPublication, false);
        enable(editionsHome, false);
        enable(dateOfPublication, true);
        enable(currentPage, true);
        enable(pages, false);
        enable(webSiteName, false);
        enable(webSiteUrl, true);
        enable(accessDate, false);
        enable(DOI, false);
        enable(ISBN, false);
    }

    else if(listOfDocumentsBox->currentIndex() == 6){		// web site

        for(auto i = authorsNameWidgetList.begin() ; i != authorsNameWidgetList.end() ; i++){
            enable(*i, true);
        }
        for(auto i = editorsNameWidgetList.begin() ; i != editorsNameWidgetList.end() ; i++){
            enable(*i, false);
        }
        for(auto i = translatorsNameWidgetList.begin() ; i != translatorsNameWidgetList.end() ; i++){
            enable(*i, false);
        }

        enable(title, true);
            enable(dissertationTypeBox, false);
        enable(institution, false);
        enable(chapter, false);
        enable(journal, false);
        enable(issueNumberForJournal, false);
        enable(volumNumber, false);
        enable(volumTitle, false);
        enable(editionNumber, false);
        enable(countryOfPublication, false);
        enable(editionsHome, false);
        enable(dateOfPublication, true);
        enable(currentPage, false);
        enable(pages, false);
        enable(webSiteName, true);
        enable(webSiteUrl, true);
        enable(accessDate, true);
        enable(DOI, false);
        enable(ISBN, false);
    }
// to be copied and adapted when a new document type is emplemented
#if 0

    else if(listOfDocumentsBox->currentIndex() == X){	// XXXXXXXX

        for(auto i = authorsNameWidgetList.begin() ; i != authorsNameWidgetList.end() ; i++){
            enable(*i, true);
        }
        for(auto i = editorsNameWidgetList.begin() ; i != editorsNameWidgetList.end() ; i++){
            enable(*i, false);
        }
        for(auto i = translatorsNameWidgetList.begin() ; i != translatorsNameWidgetList.end() ; i++){
            enable(*i, false);
        }

        enable(title, );
            enable(dissertationTypeBox, );
        enable(institution, );
        enable(chapter, );
        enable(journal, );
        enable(issueNumberForJournal, );
        enable(volumNumber, );
        enable(volumTitle, );
        enable(editionNumber, );
        enable(countryOfPublication, );
        enable(editionsHome, );
        enable(dateOfPublication, );
        enable(currentPage, );
        enable(pages, );
        enable(webSiteName, );
        enable(webSiteUrl, );
        enable(accessDate, );
        enable(DOI, );
        enable(ISBN, );
    }

#endif

}

void CitatoGui::closeEvent(QCloseEvent *event){
    // save settings everytime the main window is closed
    saveSettings();
    event->accept();
}

void CitatoGui::saveSettings(){
    // save language and geometry
    QSettings settings("Citato_experimental", "citato_main_experimental");
    settings.beginGroup("main");
    settings.setValue("language", QVariant(listOfLanguagesBox->currentIndex()));
    settings.setValue("geometry", QVariant(saveGeometry()));
    settings.setValue("specialFormatType", QVariant(listOfFormatsBox->currentIndex()));

    if(chicagoFootnoteStyleAction->isChecked()){
        settings.setValue("citationStyle", QVariant("chicagoFootnote"));
    }
    else if(chicagoInTextStyleAction->isChecked()){
        settings.setValue("citationStyle", QVariant("chicagoInText"));
    }
    else{
        settings.setValue("citationStyle", QVariant("chicagoFootnote"));
    }

    settings.endGroup();
}


void CitatoGui::loadSettings()
{
    QSettings settings("Citato_experimental", "citato_main_experimental");
    settings.beginGroup("main");

    // language settings
    int lng = settings.value("language").toInt();
    listOfLanguagesBox->setCurrentIndex(lng);

    // location of citato on the screen and the window size
    QByteArray geo = settings.value("geometry").toByteArray();
    restoreGeometry(geo);

    // format of special fields (title/journal)
    int format = settings.value("specialFormatType").toInt();
    listOfFormatsBox->setCurrentIndex(format);

    // citation style
    QString citationStyle= settings.value("citationStyle").toString();
    if(citationStyle == "chicagoFootnote") { chicagoFootnoteStyleAction->setChecked(true); }
    else if(citationStyle == "chicagoInText") { chicagoInTextStyleAction->setChecked(true); }
    else { chicagoFootnoteStyleAction->setChecked(true); }

    // theme
    QString theme = settings.value("Theme").toString();
    if(theme == "native") { nativeThemeAction->setChecked(true); }
    else if(theme == "style_dark_blue") { darkBlueThemeAction->setChecked(true); changeTheme(); }
    else if(theme == "style_light_blue") { lightBlueThemeAction->setChecked(true); changeTheme(); }
    else if(theme == "style_dark_orange") { darkOrangeThemeAction->setChecked(true); changeTheme(); }
    else if(theme == "style_light_orange") { lightOrangeThemeAction->setChecked(true); changeTheme(); }
    else if(theme == "style_dark_green") { darkGreenThemeAction->setChecked(true); changeTheme(); }
    else if(theme == "style_light_green") { lightGreenThemeAction->setChecked(true); changeTheme(); }
    else { nativeThemeAction->setChecked(true); changeTheme(); }


    settings.endGroup();
}

// check for updates
void CitatoGui::updateCitato(bool warningOn)
{
    QNetworkConfigurationManager net;
    if(net.isOnline()){		// verify if there is an internet connection

        const QString URL = "https://raw.githubusercontent.com/SZinedine/citato/master/updates.json";
        QSimpleUpdater *up = QSimpleUpdater::getInstance();

        up->setModuleVersion(URL, qApp->applicationVersion());
        up->setNotifyOnFinish(URL, false);
        up->setNotifyOnUpdate(URL, true);
        up->setUseCustomAppcast(URL, false);
        up->setDownloaderEnabled(URL, true);

        up->checkForUpdates(URL);
    }
    else{
        if(warningOn){
            QMessageBox::warning(this, "Pas de connexion internet",
                                 "Citato ne détecte aucune connexion internet sur cette machine.\n"
                                 "Veillez vous connecter à internet pour télécharger la dernière version de Citato.");
        }
    }
}

void CitatoGui::verifyLastUpdate()
{
    QDate today = QDate::currentDate();

    QSettings settings("Citato_experimental", "citato_main_experimental");

    if(!settings.childGroups().contains("update")){
        settings.beginGroup("update");
        settings.setValue("last checked update", QVariant(today));
        settings.endGroup();
        updateCitato(false);
        return;
    }

    settings.beginGroup("update");
    QDate lastTime = settings.value("last checked update").toDate();

    if(lastTime.daysTo(today) >= 7){	// try to update every week
        updateCitato(false);   // without warning about internet connection
        settings.setValue("last checked update", QVariant(today));
    }

    settings.endGroup();
}


void CitatoGui::reportBug()
{
    QString bug;
    bug = "Merci de nous rapporter toute erreur que vous aurez trouvée à cette adresse :<br>"
          "<a href=\"mailto:saibi.zineddine@yahoo.com\"> <center>saibi.zineddine@yahoo.com</center></a><br>"
          "Voici les informations à attacher à votre message :<br>"
          "- La note où l’erreur se trouver (faites un copier/coller) ;<br>"
          "- La langue sélectionnée pour générer la note ;<br>"
          "- Le type de document (livre, chapitre, article scientifique…).<br><br>"
          "Nous vous remercions pour votre aide."
            ;

    QMessageBox::information(this, "Report a Bug", bug);
}


void CitatoGui::aboutCitato()
{
    QString about;
    about = "<b>Program:</b>\t Citato <br>"
            "<b>Version:</b> "+CITATOVERSION + "<br>" +
            "<b>Author:</b>  Zineddine Saïbi<br>"
            "<b>Contact:</b> saibi.zineddine@outlook.com<br>"

            "<br><b>Description:</b><br>"
            "Citato is a program that generates a citation and biography entries from user input "
            "according to the Chicago Manual of Style, 16th edition, with some litle modifications."
            "It supports three languages: English, French and Arabic.<br>"

            "<br><b>Licence:</b><br>"
            "Citato is free software; you can redistribute it and/or "
            "modify it under the terms of the GNU General Public License "
            "as published by the Free Software Foundation; either version 2 "
            "of the License, or (at your option) any later version.<br>"

            "<br>This program is distributed in the hope that it will be useful, "
            "but WITHOUT ANY WARRANTY; without even the implied warranty of "
            "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the "
            "GNU General Public License for more details. <br>"
            ;

    QMessageBox::about(this, "About Citato", about);

}


CitatoGui::~CitatoGui()
{
    if(composer){ delete composer; }
    if(element) { delete element; }
}
