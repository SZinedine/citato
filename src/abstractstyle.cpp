#include "abstractstyle.h"
#include <QDebug>

AbstractStyle::AbstractStyle(DocumentInfo &comp, int documentType, int lang, int format)
{
    elem = new DocumentInfo(comp);
    document = documentType;
    language = lang;
    formatType = format;
}


void AbstractStyle::callAppropriateDocument()
{
    // call the apropriate functions according to the selected document type

    // choose the document type
    /*		1. book
     * 		2. chapter of a book
     * 		3. scientific article
     * 		4. newpaper article
     * 		5. dissertations (PhD, master..)
     * 		6. web site
     * 		7. interventions (seminars..)
     * ____________________________
     * 		5. magazine article
     * 		6. dictionary
     * 		7. encyclopedia
     */

    switch (document) {
    case 1:
//        qDebug() << "Generate a book citation";
        bookCitation();
        bookBibliography();
        break;
    case 2:
//        qDebug() << "Generate a chapter of a book citation";
        chapterCitation();
        chapterBibliography();
        break;
    case 3:
//        qDebug() << "Generate a journal article citation";
        scientificArticleCitation();
        scientificArticleBibliography();
        break;
    case 4:
//        qDebug() << "Generate a newspaper article citation";
        newspaperArticleCitation();
        newspaperArticleBibliography();
        break;
    case 5:
//        qDebug() << "Generate a dissertation citation";
        dissertationCitation();
        dissertationBibliography();
        break;
    case 6:
//        qDebug() << "Generate a web site article citation";
        webSiteCitation();
        webSiteBibliography();
        break;
    default:
        qDebug() << "Error. the document type you are asking about is not available.";
        citation = "ERROR";
        bibliography = "ERROR";
        break;
    }

}
