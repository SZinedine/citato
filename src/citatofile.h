#ifndef CITATOFILE_H
#define CITATOFILE_H

#include <QWidget>
#include <QString>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QTreeWidgetItem>
#include <memory>
#include <QUrl>
#include "documentinfo.h"
/*
 * STRUCTURE OF A CITATO SINGLE DOCUMENT:
 * 	A Json object which contains 2 objects:
 * 		"__META__" : contains info about the document itself
 * 		"Information" : contains info about the saved document (title...)
 */

class CitatoFile
{
public:
    CitatoFile(DocumentInfo *docInfo, QWidget *p=Q_NULLPTR);
    CitatoFile(QJsonDocument *jsDoc, QWidget *p=Q_NULLPTR);
    CitatoFile(const CitatoFile &other);

    inline DocumentInfo *getDocumentInfo(){ return documentInfo; }
    inline QJsonDocument *getJsonDocument(){ return jsonDocument; }
    void saveToFile();
    static void saveToFile(QJsonDocument *js);
    static QJsonDocument loadFromFile();
    static QJsonArray fromNameComponentsToArray(NameComponents name);
    static NameComponents fromArrayToNameComponents(QJsonArray nameArray);
    static QJsonArray fromListOfNamesToArray(QList<NameComponents> names);
    static QList<NameComponents> fromArrayToListOfNames(QJsonArray jsArray);
    static QTreeWidgetItem fromJsonDocumentToTreeWidgetItem(QJsonDocument *doc);
    static QTreeWidgetItem fromJsonDocumentToTreeWidgetItem(QJsonObject *obj);
    static QJsonDocument fromFileToJsonDocument(QFile *f);
    static QJsonObject fromFileToJsonObject(QFile *f);
    bool isValid();
    static bool isValid(QJsonDocument *j);
    static bool isValid(const QJsonObject *o);
    static bool isValidWithWarning(QJsonDocument *j);
    static bool isValidWithWarning(QFile *f);
    inline bool isValidWithWarning() { return isValidWithWarning(jsonDocument); }
    static DocumentInfo fromJsonDocumentToDocumentInfo(QJsonDocument *js);
    static QJsonDocument fromDocumentInfoToJsonDoc(DocumentInfo *docInfo);


private:
    void fromJsonDocumentToDocumentInfo();
    void fromDocumentInfoToJsonDoc();

    QWidget *parent;
    QJsonDocument *jsonDocument;
    DocumentInfo *documentInfo;
};

#endif // CITATOFILE_H
