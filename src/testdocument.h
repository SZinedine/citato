#ifndef TESTDOCUMENT_H
#define TESTDOCUMENT_H

#include <QWidget>
#include <QString>
#include "documentinfo.h"

class TestDocument
{
public:
    TestDocument( DocumentInfo doc, const int typeDoc, int language, const QWidget *parent=0);
    ~TestDocument();

    void testBook();
    void testChapter();
    void testScientificArticle();
    void testNewspaperArticle();
    void testDissertation();
    void testWebSite();
    bool isError();
    QString getErrorMessage();

private:
    QWidget *parent;
    DocumentInfo *document;
    QString listOfMissingFields;
    bool errors;
    QString errorMessage;
    int language;
    void addTitle();
    void addChapter();
    void addDissertationType();
    void addYear();
    void addCountryOfPublication();
    void addJournal();
    void addInstitution();
    void addLocation();
    void addEditionsHome();
    void addWebSiteUrl();
};

#endif // TESTDOCUMENT_H
