#include "citatogui.h"
#include <QApplication>
#include <QDebug>

#define CITATO_VERSION "0.0.14"
#define CITATO_TITLEBAR "Citato [non final version] v."
#define CITATO_NAME "Citato"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationName(CITATO_NAME);
    app.setApplicationDisplayName(CITATO_NAME);
    app.setApplicationVersion(CITATO_VERSION);

    app.setWindowIcon(QIcon(":images/citato.png"));

    CitatoGui CitatoMainWindow(CITATO_VERSION);
    CitatoMainWindow.setWindowTitle(QString(CITATO_TITLEBAR) + QString(CITATO_VERSION));
    CitatoMainWindow.show();

    return app.exec();
}
