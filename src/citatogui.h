#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QScrollArea>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>
#include <QComboBox>
#include <QRadioButton>
#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QStatusBar>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QString>
#include <QList>
#include <QFont>
#include <QFontDatabase>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QNetworkConfigurationManager>
#include <QSimpleUpdater.h>
#include <QDate>
#include "namewidget.h"
#include "namecomponents.h"
#include "documentinfo.h"
#include "testdocument.h"
#include "citatodatabase.h"
#include "citatofile.h"
#include "citatomanagergui.h"
#include "abstractstyle.h"
#include "chicagofootnotestyle.h"
#include "chicagointextstyle.h"
#include "field.h"

#define FONT_ROMAN "Liberation Serif, Regular"
#define FONT_ARABIC "Simplified Arabic, Regular"

class CitatoGui : public QMainWindow
{
    Q_OBJECT

public:
    CitatoGui(QString version, QWidget *parent = 0);
    ~CitatoGui();
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);

private slots:
    void changeLanguage();
    void changeDocument();
    void clearAllWidgets();
    void callComposer();
    void addAuthorField();
    void deleteAuthorField();
    void addEditorField();
    void deleteEditorField();
    void addTranslatorField();
    void deleteTranslatorField();
    void openCitatoManager();
    void copyCitation();			// copy the result to the system clipboard
    void copyBibliography();
    void saveDocument();
    void loadDocument();
    void displayDocument(QJsonDocument *js);
    void displayDocument(DocumentInfo &info);
    void saveSettings();		// save the used language
    void loadSettings();
    void closeEvent(QCloseEvent *event);	// reemplement an existing member
    void reportBug();
    void aboutCitato();
    void verifyLastUpdate();
    void updateCitato(bool warningOn=true);
    void changeTheme();

private:
    // constructor abstractions
    void setupCentralZone();
    void setupStyle();
    void setupLayout();
    void setupMenu();
    void setupSignalsSlots();
    void setupKeyboardShortcuts();
    QList<NameComponents> fromNameWidgetListToNameComponentsList(QList<NameWidget*> lst);
    void constructElement();		// construct the element by initializing every variable of the class by the user input
    inline void allocElement() { if(!element) element = new DocumentInfo(listOfLanguagesBox->currentIndex()); }
    inline void freeElement() { if(element) element = nullptr; }
    inline void freeComposer() { if(composer) composer = nullptr; }
    inline void enable(QWidget *w, bool yes) { w->setVisible(yes); }


    QList<NameWidget*> authorsNameWidgetList;
    QList<NameWidget*> editorsNameWidgetList;
    QList<NameWidget*> translatorsNameWidgetList;
    QVBoxLayout *authorsWidgetsLayout;
    QVBoxLayout *editorsWidgetsLayout;
    QVBoxLayout *translatorsWidgetsLayout;
    AbstractStyle *composer = nullptr;
    DocumentInfo *element = nullptr;		// class that stores and treats the user input for each field

    /** Widgets **/
    QWidget *mainWidget;		// contains all other widgets
    QScrollArea *fieldsContainerScrollArea;
    QWidget *widgetOfScrollArea;			// inside the scroll area
    QTextEdit *citationDisplayer;
    QTextEdit *bibliographyDisplayer;
    QLabel *citationDisplayerLabel;
    QLabel *bibliographyDisplayerLabel;
    CitatoManagerGui *citatoManager;

    // Layouts
    QGridLayout *mainLayout;
    QHBoxLayout *headerLayout;
    QVBoxLayout *scrollAreaLayout;

    // buttons and comboboxes
    QPushButton *clearAllButton;
    QPushButton *generateButton;
    QPushButton *addAuthorsButton;
    QPushButton *deleteAuthorButton;
    QPushButton *addEditorsButton;
    QPushButton *deleteEditorButton;
    QPushButton *addTranslatorButton;
    QPushButton *deleteTranslatorButton;
    QPushButton *copyCitationButton;
    QPushButton *copyBibliographyButton;
    QComboBox *listOfLanguagesBox;
    QComboBox *listOfDocumentsBox;
    QComboBox *listOfFormatsBox;
    QComboBox *dissertationTypeBox;
    Field *title;
    Field *institution;
    Field *chapter;
    Field *journal;
    Field *issueNumberForJournal;
    Field *volumNumber;
    Field *volumTitle;
    Field *editionNumber;
    Field *countryOfPublication;
    Field *editionsHome;
    Field *yearOfPublication;
    Field *timeOfPublication;
    DateField *dateOfPublication;
    Field *currentPage;
    PagesField *pages;
    Field *webSiteName;
    Field *webSiteUrl;
//    Field *accessDate;
    DateField *accessDate;
    Field *DOI;
    Field *ISBN;

    // Menu
    QMenu *menuFile;
//        QAction *openCitatoManagerAction;
        QAction *openCitatoFileAction;
        QAction *saveCitatoFileAction;
        QAction *quitAction;
    QMenu *optionsMenu;
        QMenu *ChooseStyleAction;
            QActionGroup *listOfCitationStylesActionGroup;
            QAction *chicagoFootnoteStyleAction;
            QAction *chicagoInTextStyleAction;
    QMenu *chooseCitatoTheme;
        QActionGroup *listOfthemes;
            QAction *nativeThemeAction;
            QAction *darkBlueThemeAction;
            QAction *lightBlueThemeAction;
            QAction *darkOrangeThemeAction;
            QAction *lightOrangeThemeAction;
            QAction *darkGreenThemeAction;
            QAction *lightGreenThemeAction;
    QMenu *interogationMenu;
        QAction *reportBugAction;
        QAction *aboutCitatoAction;
        QAction *updateCitatoAction;

    // status bar
    QStatusBar *statusB;
    QLabel *creatorSZ;
    QString citationString;
    QString bibliographyString;
    QString CITATOVERSION;	// version of Citato
};

#endif // MAINWINDOW_H
