#ifndef NAMECOMPONENTS_H
#define NAMECOMPONENTS_H

#include <QList>
#include <QString>

// this class is used to manage the names of Authors, editors and translators

class NameComponents
{
public:
    NameComponents();
    NameComponents(QString fir, QString mid, QString fam);

    void setNames(QString first, QString middle, QString family);
    void setFirstName(QString first);
    void setMiddleName(QString middle);
    void setFamilyName(QString family);
    QString getFirstName(QString pre="", QString post="");
    QString getMiddleName(QString pre="", QString post="");
    QString getFamilyName(QString pre="", QString post="");
    QString getCitation(QString pre="", QString post="");
    QString getBibliography(QString COMMA, QString pre="", QString post="");
    bool isValid() const;
    bool isFirstName() const;
    bool isMiddleName() const;
    bool isFamilyName() const;

private:
    QString firstName = "";
    QString middleName = "";
    QString familyName = "";
};

#endif // NAMECOMPONENTS_H
