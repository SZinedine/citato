#ifndef FIELD_H
#define FIELD_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QHBoxLayout>


/*
 * the Field class is a convinience class that combine
 * a label and a LineEdit in a horizontal layout,
 * along with methods to modify their contents.
 *
 * it also plays the role of a base class to other
 * more specific widget classes.
 */
class Field : public QWidget
{
    Q_OBJECT
public:
    explicit Field(QWidget *parent = nullptr);

    inline void setLineEditText(const QString &t) { line->setText(t); }
    inline void setLineEditPlaceholder(const QString &p) { line->setPlaceholderText(p); }
    inline void setLabelText(QString l) { label->setText(l); }
    void changeText(QString labelText, QString linePlaceHolder="");
    void setLabelMaximumWidth(int labMax);
    virtual inline void clear() { line->clear(); }
    inline QString getText() { return line->text(); }
    inline QLineEdit* getLineEdit() { return line; }
    inline QLabel* getLabel() { return label; }
    inline QHBoxLayout* getLayout() { return layout; }

protected:
    QLabel *label;
    QLineEdit *line;
    QHBoxLayout *layout;
};


/*
 * a class for dealing with page widget
 * Description:
 * 	3 widgets in horizontal layout
 * 		1. label ("pages")
 * 		2. the "from" page
 * 		3. the "to" page
 */
class PagesField : public Field
{
public:
    explicit PagesField(QWidget *parent = nullptr);

    void setLineEditText(QString from, QString to);
    inline void setToPlaceholder(QString p) { toLine->setPlaceholderText(p); }
    inline void setFromPlaceHolder(QString p) { line->setPlaceholderText(p); }
    void setLineEditPlaceholders(QString from, QString to);
    inline QString getToText() { return toLine->text(); }
    inline QString getFromText() { return line->text(); }
    void changeText(QString labelText, QString fromLinePlaceHolder,
                    QString toLineHolder);
    void clear() override;


private:
    QLineEdit *toLine;
};

/*
 * the DateField class has 4 componets:
 * 	1. the label inherited from Field;
 *  2. the "line" lineEdit inherited from Field, it plays the role of "year" in this class.
 *  3. the month lineEdit.
 *  4. the day lineEdit.
 */
class DateField : public Field
{
public:
    explicit DateField(QWidget *parent = nullptr);

    void setLineEditText(const QString d, const QString m, const QString y);
    inline void setYearPlaceHolder(const QString y)  { line->setPlaceholderText(y); }
    inline void setMonthPlaceHolder(const QString m) { monthOrSeason->setPlaceholderText(m); }
    inline void setDayPlaceHolder(const QString d)   { day->setPlaceholderText(d); }
    inline void setYearText(const QString y)		 { line->setText(y); }
    inline void setMonthText(const QString m)		 { monthOrSeason->setText(m); }
    inline void setDayText(const QString d)			 { day->setText(d); }
    void setLineEditPlaceholders(const QString y, const QString m, const QString d);
    inline QString getYear()  { return line->text(); }
    inline QString getMonth() { return monthOrSeason->text(); }
    inline QString getDay()   { return day->text(); }
    void changeText(const QString &labelText, const QString &y,
                    const QString &m, const QString &d);

    void clear() override;

private:
    // the lineEdit of "year" is the one which is inherited from Field
    QLineEdit *monthOrSeason;
    QLineEdit *day;
};

#endif // FIELD_H
