#include "documentinfo.h"
#include <QDebug>

DocumentInfo::DocumentInfo()
{

}


DocumentInfo::DocumentInfo(QString languageString)
{
    setDocumentLanguage(languageString);
}

DocumentInfo::DocumentInfo(int languageIndex)
{
    setDocumentLanguage(languageIndex);
}

DocumentInfo::DocumentInfo(const DocumentInfo &other)
{
    documentLanguage = other.documentLanguage;
    documentLanguageInt = other.documentLanguageInt;
    documentType = other.documentType;

    authors = other.authors;
    title = other.title;
    dissertationType = other.dissertationType;
    dissertationTypeInt = other.dissertationTypeInt;
    institution = other.institution;
    chapter = other.chapter;
    journal = other.journal;
    issueNumberJournal = other.issueNumberJournal;
    editors = other.editors;
    translators = other.translators;
    editionNumber = other.editionNumber;
    editionsHome = other.editionsHome;
    volumNumber = other.volumNumber;
    volumTitle = other.volumTitle;
    location = other.location;
    yearOfPublication = other.yearOfPublication;
    monthOfPublication = other.monthOfPublication;
    dayOfPublication = other.dayOfPublication;
    timePublication = other.timePublication;		// deprecated
    currentPage = other.currentPage;
    pageFrom = other.pageFrom;
    pageTo = other.pageTo;
    webSiteName = other.webSiteName;
    webSiteUrl = other.webSiteUrl;
//    accessDate = other.accessDate;
    accessYear = other.accessYear;
    accessMonth = other.accessMonth;
    accessDay = other.accessDay;
    DOI = other.DOI;
    ISBN = other.ISBN;
}


// SET THE DOCUMENT LANGUAGE IF IT ISN'T DEFINED YET

void DocumentInfo::setDocumentLanguage(const QString languageString)
{

    if(languageString == "en" ||
            languageString == "En" ||
            languageString == "english" ||
            languageString == "English" ||
            languageString == "anglais" ||
            languageString == "Anglais"){
        documentLanguage = "en";
        documentLanguageInt = 1;
    }

    else if(languageString == "fr" ||
            languageString == "Fr" ||
            languageString == "french" ||
            languageString == "French" ||
            languageString == "français" ||
            languageString == "Français"){
        documentLanguage = "fr";
        documentLanguageInt = 2;
    }

    else if(languageString == "ar" ||
            languageString == "Ar" ||
            languageString == "arabic" ||
            languageString == "Arabic" ||
            languageString == "arabe" ||
            languageString == "Arabe"){
        documentLanguage = "ar";
        documentLanguageInt = 3;
    }
    else{
        documentLanguage = "None";
        documentLanguageInt = 0;
    }
}

void DocumentInfo::setDocumentLanguage(const int languageIndex)
{
    documentLanguageInt = languageIndex;

    switch (languageIndex) {
    case 1:
        documentLanguage = "en";
        break;
    case 2:
        documentLanguage = "fr";
        break;
    case 3:
        documentLanguage = "ar";
        break;
    default:
        documentLanguage = "None";
        break;
    }
}


QString DocumentInfo::getDocumentTypeStr() const
{
    switch (documentType) {
    case 1:
        return QString("book");
        break;
    case 2:
        return QString("chapter");
        break;
    case 3:
        return QString("scientific article");
        break;
    case 4:
        return QString("newspaper");
        break;
    case 5:
        return QString("dissertation");
        break;
    case 6:
        return QString("web site");
        break;
    default:
        return QString("book");
        break;
    }
}


/******************************************************************/
/************************** SETTERS *******************************/
/******************************************************************/

void DocumentInfo::setDissertationType(int dissIndex){
    dissertationTypeInt = dissIndex;

    QString licence = "";
    QString master = "";
    QString magister = "";
    QString doctorate = "";

    switch (documentLanguageInt) {
    case 1:
        licence = "licence dissertation";
        master = "master's dissertation";
        magister = "magister's dissertation";
        doctorate = "PhD thesis";
        break;
    case 2:
        licence = "mémoire de licence";
        master = "mémoire de master";
        magister = "mémoire de magistère";
        doctorate = "thèse de doctorat";
        break;
    case 3:
        licence = "مذكرة ليسانس";
        master = "مذكرة ماستر";
        magister = "رسالة ماجستير";
        doctorate = "أطروحة دكتوراه";
        break;
    }


    switch (dissIndex) {
    case 1: dissertationType = licence; break;
    case 2: dissertationType = master; break;
    case 3: dissertationType = magister; break;
    case 4: dissertationType = doctorate; break;
    default:dissertationType = ""; break;
    }
}




void DocumentInfo::setWebSiteUrl(QString ws){
    QString x = ws.simplified();
    if(x.endsWith("/")) {
        webSiteUrl = x.remove(x.length()-1, 1);
    }
    else{
        webSiteUrl = x;
    }
}



/******************************************************************/
/************************** GETTERS *******************************/
/******************************************************************/

QList<NameComponents> DocumentInfo::getAuthors(){
    QList<NameComponents> x = authors;
    return x;
}

QList<NameComponents> DocumentInfo::getAuthorsList(){
    QList<NameComponents> x = authors;
    return x;
}

int DocumentInfo::getAuthorsListLength() const {
    return authors.length();
}

QString DocumentInfo::getTitle(QString pre, QString post){
    if(isTitle()){
        QString x = toTitleCase(title);
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

QString DocumentInfo::getDissertationType(QString pre, QString post){
    if(isDissertationType()){
        QString x = dissertationType;
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

int DocumentInfo::getDissertationTypeInt() const {
    return dissertationTypeInt;
}

QString DocumentInfo::getInstitution(QString pre, QString post) {
    if(isInstitution()){
        QString x = toTitleCase(institution);
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}


QString DocumentInfo::getChapter(QString pre, QString post) {
    if(isChapter()){
        QString x = toTitleCase(chapter);
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

QString DocumentInfo::getJournal(QString pre, QString post) {
    if(isJournal()){
        QString x = toTitleCase(journal);
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

QString DocumentInfo::getIssueNumberJournal(QString pre, QString post){
    if(isIssurNumberJournal()){
        QString x = issueNumberJournal;
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

QList<NameComponents> DocumentInfo::getEditors() const{
    QList<NameComponents> x = editors;
    return x;
}

QList<NameComponents> DocumentInfo::getEditorsList() const{
    QList<NameComponents> x = editors;
    return x;
}

int DocumentInfo::getEditorsListLength() const {
    return editors.length();
}


QList<NameComponents> DocumentInfo::getTranslator() const{
    QList<NameComponents> x = translators;
    return x;
}

QList<NameComponents> DocumentInfo::getTranslatorsList() const {
    QList<NameComponents> x = translators;
    return x;
}

int DocumentInfo::getTranslatorsListLength() const {
    return translators.length();
}


QString DocumentInfo::getEditionNumber(QString pre, QString post) {
    if(isEditionNumber()){
        QString x =  editionNumber;
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

QString DocumentInfo::getEditionsHome(QString pre, QString post) {
    if(isEditionsHome()){
        QString x = toTitleCase(editionsHome);
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

QString DocumentInfo::getVolumNumber(QString pre, QString post) {
    if(isVolumNumber()){
        QString x = volumNumber;
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

QString DocumentInfo::getVolumTitle(QString pre, QString post) {
    if(isVolumTitle()){
        QString x = volumTitle;
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

QString DocumentInfo::getLocation(QString pre, QString post) {
    if(isLocation()){
        QString x = toTitleCase(location);
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

QString DocumentInfo::getYearOfPublication(QString pre, QString post) {
    if(isYearOfPublication()){
        QString x = yearOfPublication;
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

QString DocumentInfo::getMonthOfPublication(QString pre, QString post)
{
    if(isMonthOfPublication()){
        QString x = monthOfPublication;
        return x.prepend(pre).append(post);
    } else { return QString(""); }
}

QString DocumentInfo::getDayOfPublication(QString pre, QString post)
{
    if(isDayOfPublication()){
        QString x = dayOfPublication;
        return x.prepend(pre).append(post);
    } else { return QString(""); }
}

QString DocumentInfo::getMonthAndYearOfPublication(QString pre, QString post)
{
    if(isMonthOfPublication() || isYearOfPublication()){
        QString d = "";
        d.append(monthOfPublication.simplified());
        if(isMonthOfPublication() && isYearOfPublication()){ d.append(" "); }
        d.append(yearOfPublication.simplified());
        return d.prepend(pre).append(post);
    }
    else { return QString(""); }
}

QString DocumentInfo::getMonthAndDayOfPublication(QString pre, QString post)
{
    if(isMonthOfPublication() && isDayOfPublication()){
       if(documentLanguageInt == 1){
           return QString(monthOfPublication + " " + dayOfPublication).prepend(pre).append(post);
       } else { return QString(dayOfPublication + " " + monthOfPublication).prepend(pre).append(post); }
    }
    else if(isMonthOfPublication() && !isDayOfPublication()){
        return monthOfPublication.prepend(pre).append(post);
    }
    else { return QString(""); }
}


QString DocumentInfo::getCurrentPage(QString pre, QString post){
    if(isCurrentPage()){
        QString x = currentPage;
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

QString DocumentInfo::getPageFrom(QString pre, QString post) {
    if(isPageFrom()) {
        QString x = pageFrom;
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

QString DocumentInfo::getPageTo(QString pre, QString post) {
    if(isPageTo()){
        QString x = pageTo;
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

QString DocumentInfo::getPages(QString pre, QString post) {
    if(isPageTo() && isPageFrom()){
        QString p = getPageFrom() + getPageTo("-");
        return p.prepend(pre).append(post);
    }
    else if(isPageFrom() && !isPageTo()){
        return getPageFrom(pre, post);
    }
    else if(!isPageFrom() && isPageTo()){
        return getPageTo(pre, post);
    }
    else{
        return QString("");
    }
}

QString DocumentInfo::getWebSiteName(QString pre, QString post) {
    if(isWebSiteName()){
        QString x = webSiteName;
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

QString DocumentInfo::getWebSiteUrl(QString pre, QString post) {
    if(isWebSiteUrl()){
        QString x = webSiteUrl;
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

QString DocumentInfo::getAccessDate(QString pre, QString post){
    QString x = formatDate(accessDay, accessMonth, accessYear);
    qDebug() << x;
    x = x.simplified().prepend(pre).append(post);
    if(!x.isEmpty()){
        return x;
    }
    else { return QString(""); }
}

QString DocumentInfo::getAccessDay(QString pre, QString post){
    if(isAccessDay()){
        QString x = accessDay;
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

QString DocumentInfo::getAccessYear(QString pre, QString post){
    if(isAccessYear()){
        QString x = accessYear;
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

QString DocumentInfo::getAccessMonth(QString pre, QString post){
    if(isAccessMonth()){
        QString x = accessMonth;
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

QString DocumentInfo::getAccessMonthAndYear(QString pre, QString post){
    if(isAccessMonth() || isAccessYear()){
        QString d = "";
        d.append(accessMonth.simplified());
        d.append(" ");
        d.append(accessYear);
        d = d.simplified();
        return d.prepend(pre).append(post);
    }
    else{ return QString(""); }
}


QString DocumentInfo::getDOI(QString pre, QString post){
    if(isDOI()){
        QString x = DOI;
        if(!x.startsWith("doi")){
            x = "doi: " + x;
        }
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

QString DocumentInfo::getISBN(QString pre, QString post){
    if(isISBN()){
        QString x = ISBN;
        return x.prepend(pre).append(post);
    }
    else{ return QString(""); }
}

/******************************************************************/
/****************** INDEPENDENT FUNCTIONS *************************/
/******************************************************************/

QString DocumentInfo::getPublicationDate(QString pre, QString post)
{
    return formatDate(dayOfPublication, monthOfPublication, yearOfPublication, pre, post);
}


// format the publication date
QString DocumentInfo::formatDate(QString d, QString m, QString y, QString pre, QString post)
{
    QString date = "";
    if(documentLanguageInt == 1) {

        if(!d.isEmpty() && !m.isEmpty() && !y.isEmpty()){
            date = m.simplified() + " " + d.simplified() + ", " + y.simplified();
        }
        else if((d.isEmpty() && m.isEmpty() && !y.isEmpty()) ||
                (!d.isEmpty() && m.isEmpty() && !y.isEmpty()) ){
            date = y.simplified();
        }
        else if(d.isEmpty() && !m.isEmpty() && !y.isEmpty()){
            date = m.simplified() + " " + y.simplified();
        }
    }

    else if(documentLanguageInt == 2 || documentLanguageInt == 3){

        if(!d.isEmpty() && !m.isEmpty() && !y.isEmpty()){
            date = d.simplified() + " " + m.simplified() + " " + y.simplified();
        }
        else if((d.isEmpty() && m.isEmpty() && !y.isEmpty()) ||
                (!d.isEmpty() && m.isEmpty() && !y.isEmpty()) ){
            date = y.simplified();
        }
        else if(d.isEmpty() && !m.isEmpty() && !y.isEmpty()){
            date = m.simplified() + " " + y.simplified();
        }
    }

    else{
        date = "*** ERROR IN DATE LANGUAGE ***";
    }

    return date.prepend(pre).append(post);
}

void DocumentInfo::clear(){
    documentLanguage = "";
    documentLanguageInt = 0;
    documentType = 0;

    authors.clear();
    title = "";
    dissertationType = "";
    dissertationTypeInt = 0;
    institution = "";
    chapter = "";
    journal = "";
    issueNumberJournal = "";
    editors.clear();
    translators.clear();
    editionNumber = "";
    editionsHome = "";
    volumNumber = "";
    location = "";
    yearOfPublication = "";
    monthOfPublication = "";
    dayOfPublication = "";
    timePublication = "";
    currentPage = "";
    pageFrom = "";
    pageTo = "";
    webSiteName = "";
    webSiteUrl = "";
//    accessDate = "";
    accessYear = "";
    accessMonth = "";
    accessDay = "";
    DOI = "";
    ISBN = "";
}



QString DocumentInfo::toTitleCase(QString str){
    // return a titleCased copy of a string
    QString tmp;
    str = str.simplified();

    for(auto i = str.begin() ; i != str.end() ; i++){
        if(i == str.begin()){	// capitalize the first letter
            tmp.append(i->toUpper());
            continue;
        }

        if(i->isSpace()){
            tmp.append(*i);

            if(i + 1 < str.end() && i != str.begin()){	// capitalize the letter after a space
                i++;
                tmp.append(i->toUpper());
                continue;
            }
        }

        if(!i->isSpace() && i != str.begin()){
            tmp.append(*i);
        }
    }

    return tmp;
}

