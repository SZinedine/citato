#ifndef CHICAGOINTEXTSTYLE_H
#define CHICAGOINTEXTSTYLE_H

#include "chicagofootnotestyle.h"

class ChicagoInTextStyle : public ChicagoFootnoteStyle
{
public:
    ChicagoInTextStyle(DocumentInfo &comp, int documentType, int language, int formatType=1);

protected:

    void bookCitation() override;							// book
    void bookBibliography() override;
    void chapterCitation() override;							// chapter of a book
    void chapterBibliography() override;
    void scientificArticleCitation() override;				// scientific article (from a journal)
    void scientificArticleBibliography() override;
    void newspaperArticleCitation() override;				// article from a newspaper
    void newspaperArticleBibliography() override;
    void dissertationCitation() override;					// dissertation
    void dissertationBibliography() override;
    void webSiteCitation() override;							// web site
    void webSiteBibliography() override;

    QString listCitationNames_familyNameOnly(QList<NameComponents> lst, QString pre="", QString post="", int maximumNumberOfNames=10) const;
    QString singleCitationName_familyNameOnly(NameComponents n, QString pre="", QString post="") const ;
};

#endif // CHICAGOINTEXTSTYLE_H
