#include "chicagointextstyle.h"
#include <iostream>
#include <QDebug>

ChicagoInTextStyle::ChicagoInTextStyle(DocumentInfo &comp, int documentType, int language, int formatType)
    : ChicagoFootnoteStyle(comp, documentType, language, formatType)
{
    adaptLanguage();
    callAppropriateDocument();
}

void ChicagoInTextStyle::bookCitation()
{
    QString c;

    if(elem->isAuthors()) {
        c.append(listCitationNames_familyNameOnly(elem->getAuthors(), "", "", 4));
    }
    else if(elem->isEditors() && !elem->isAuthors()){
        c.append(listCitationNames_familyNameOnly(elem->getEditors(), "", "", 4));
    }

    c.append(elem->getYearOfPublication(" "));
    if((elem->isAuthors() || elem->isEditors() || elem->isYearOfPublication()) && elem->isCurrentPage()) { c.append(COMMA); }
    c.append(elem->getCurrentPage());
    c = c.simplified();

    if(!c.isEmpty()) { citation = c.prepend("(").append(")"); }
    else { citation = ""; }
}

void ChicagoInTextStyle::bookBibliography()
{
    QString nameBlock;
    {
        if(elem->isAuthors() && elem->isEditors()){

            nameBlock.append(listBibliographyNames(elem->getAuthorsList(), "", "", 10));
            nameBlock.append(elem->getYearOfPublication(" "));
            if(elem->isYearOfPublication()) { nameBlock.append("."); }
            nameBlock.append(elem->getTitle(" " + FORM_OPEN, FORM_CLOSE));
            if(elem->isVolumNumber() && elem->isVolumTitle()){
                nameBlock.append(elem->getVolumNumber(". " + VOLNUM));
                nameBlock.append(elem->getVolumTitle(". " + FORM_OPEN, FORM_CLOSE));
            }
            nameBlock.append(listCitationNames(elem->getEditorsList(), ". " +ED_BIB+" "));
        }
        else {
            if(elem->isEditors() && !elem->isAuthors()){
                nameBlock.append(listBibliographyNames(elem->getEditorsList(), "", " " + ED_CIT, 10));
                nameBlock.append(elem->getYearOfPublication(" "));
                if(elem->isYearOfPublication()) { nameBlock.append("."); }
                nameBlock.append(elem->getTitle(" " + FORM_OPEN, FORM_CLOSE));
            }
            else if(elem->isAuthors() && !elem->isEditors()){
                nameBlock.append(listBibliographyNames(elem->getAuthorsList(), "", "", 10));
                nameBlock.append(elem->getYearOfPublication(" "));
                if(elem->isYearOfPublication()) { nameBlock.append("."); }
                nameBlock.append(elem->getTitle(" " + FORM_OPEN, FORM_CLOSE));
            }
            else{		// if the author and editor are not defined, make the title go first
                nameBlock.append(elem->getYearOfPublication());
                if(elem->isYearOfPublication()) { nameBlock.append(". "); }
                nameBlock.append(elem->getTitle(FORM_OPEN, FORM_CLOSE));
            }

            if(elem->isVolumNumber() && elem->isVolumTitle()){
                nameBlock.append(elem->getVolumNumber(". " + VOLNUM));
                nameBlock.append(elem->getVolumTitle(". " + FORM_OPEN, FORM_CLOSE));
            }
        }

        nameBlock.append(listCitationNames(elem->getTranslatorsList(), ". " + TRANS_BIB, "", 10));
    }

    // editions number and volumes
    {
        if(language == 3){
            nameBlock.append(elem->getEditionNumber(". " + EDNUM));
            if(!elem->isVolumTitle()) { nameBlock.append(elem->getVolumNumber(". " + VOLNUM)); }
        }
        else{
            if(elem->isEditionNumber()) { nameBlock.append(". "); }
            nameBlock.append(elem->getEditionNumber("", EDNUM));
            if(elem->isVolumNumber() && !elem->isEditionNumber() && !elem->isVolumTitle()) { nameBlock.append("."); }
            if(!elem->isVolumTitle()) { nameBlock.append(elem->getVolumNumber(" " + VOLNUM)); }
        }
    }

    nameBlock = nameBlock.simplified();
    if(!nameBlock.endsWith(".")){ nameBlock.append(". "); }

    QString secondBlock = " ";
    {
        secondBlock.append(elem->getLocation());

        if(elem->isLocation() && elem->isEditionsHome()) { secondBlock.append(": "); }
        secondBlock.append(elem->getEditionsHome());


        if( (!secondBlock.endsWith(". ") || !secondBlock.endsWith("."))) { secondBlock.append(". "); }

        secondBlock.append(elem->getWebSiteUrl("", "."));
    }

    bibliography = nameBlock + secondBlock;
    bibliography = bibliography.simplified();
}

void ChicagoInTextStyle::chapterCitation()
{
    QString c;

    if(elem->isAuthors()) {
        c.append(listCitationNames_familyNameOnly(elem->getAuthors(), "", "", 4));
    }
    else if(elem->isEditors() && !elem->isAuthors()){
        c.append(listCitationNames_familyNameOnly(elem->getEditors(), "", "", 4));
    }

    c.append(elem->getYearOfPublication(" "));
    if((elem->isAuthors() || elem->isEditors() || elem->isYearOfPublication()) && elem->isCurrentPage()) { c.append(COMMA); }
    c.append(elem->getCurrentPage());
    c = c.simplified();

    if(!c.isEmpty()) { citation = c.prepend("(").append(")"); }
    else { citation = ""; }
}


void ChicagoInTextStyle::chapterBibliography(){

    QString nameBlock;
    {
        if(elem->isAuthor() && elem->isEditor()){
            nameBlock.append(listBibliographyNames(elem->getAuthorsList()));
            nameBlock.append(elem->getYearOfPublication(" "));
            if(elem->isYearOfPublication()) { nameBlock.append("."); }

            nameBlock.append(elem->getChapter(" \"", "\""));
            nameBlock.append(elem->getTitle(". " + IN + FORM_OPEN, FORM_CLOSE));
            if(elem->isVolumNumber() && elem->isVolumTitle()){
                nameBlock.append(elem->getVolumNumber(". " + VOLNUM));
                nameBlock.append(elem->getVolumTitle(". " + FORM_OPEN, FORM_CLOSE));
            }

            nameBlock.append(listCitationNames(elem->getEditorsList(), ". " + ED_BIB + " "));
        }
        else{

            if(elem->isEditor() && !elem->isAuthor()){
                nameBlock.append(listBibliographyNames(elem->getEditorsList(), "", " " + ED_CIT, 10));
                nameBlock.append(elem->getYearOfPublication(" "));
                if(elem->isYearOfPublication()) { nameBlock.append("."); }
                nameBlock.append(elem->getChapter(" \"", "\""));
                nameBlock.append(elem->getTitle(". " + FORM_OPEN, FORM_CLOSE));
            }

            else if(elem->isAuthor() && !elem->isEditor()){
                nameBlock.append(listBibliographyNames(elem->getAuthorsList()));
                nameBlock.append(elem->getYearOfPublication(" "));
                if(elem->isYearOfPublication()) { nameBlock.append("."); }
                nameBlock.append(elem->getChapter(" \"", "\""));
                nameBlock.append(elem->getTitle(". " + IN + FORM_OPEN, FORM_CLOSE));
            }

            else{
                nameBlock.append(elem->getYearOfPublication("", ". "));
                nameBlock.append(elem->getChapter("\"", "\""));
                nameBlock.append(elem->getTitle(". " + IN + FORM_OPEN, FORM_CLOSE));
            }

            if(elem->isVolumNumber() && elem->isVolumTitle()){
                nameBlock.append(elem->getVolumNumber(". " + VOLNUM));
                nameBlock.append(elem->getVolumTitle(". " + FORM_OPEN, FORM_CLOSE));
            }
        }

        nameBlock.append(listCitationNames(elem->getTranslatorsList(), ". " + TRANS_BIB));
        nameBlock = nameBlock.simplified();
    }


    // editions number and volumes
    {
        if(language == 3){
            nameBlock.append(elem->getEditionNumber(". " + EDNUM));
            if(!elem->isVolumTitle()) { nameBlock.append(elem->getVolumNumber(". " + VOLNUM)); }
        }
        else{
            if(elem->isEditionNumber()) { nameBlock.append(". "); }
            nameBlock.append(elem->getEditionNumber("", EDNUM));
            if(elem->isVolumNumber() && !elem->isEditionNumber() && !elem->isVolumTitle()) { nameBlock.append("."); }
            if(!elem->isVolumTitle()) { nameBlock.append(elem->getVolumNumber(" " + VOLNUM)); }
        }
    }

    nameBlock = nameBlock.simplified();
    if(!nameBlock.endsWith(".")){ nameBlock.append(". "); }


    QString secondBlock = " ";
    {
        secondBlock.append(elem->getLocation());

        if(elem->isLocation() && elem->isEditionsHome()) { secondBlock.append(": "); }

        secondBlock.append(elem->getEditionsHome());

//        if((elem->isEditionsHome() || elem->isLocation()) && elem->isYearOfPublication()) { secondBlock.append(COMMA); }
//        secondBlock.append(elem->getYearOfPublication());
        secondBlock.append(elem->getPages(". "));

        if( (!secondBlock.endsWith(". ") || !secondBlock.endsWith("."))) { secondBlock.append(". "); }

        secondBlock.append(elem->getWebSiteUrl("", "."));
    }

    bibliography = nameBlock + secondBlock;
}


void ChicagoInTextStyle::scientificArticleCitation()
{
    QString c;

    if(elem->isAuthors()) {
        c.append(listCitationNames_familyNameOnly(elem->getAuthors(), "", "", 4));
    }
    else if(elem->isEditors() && !elem->isAuthors()){
        c.append(listCitationNames_familyNameOnly(elem->getEditors(), "", "", 4));
    }

    c.append(elem->getYearOfPublication(" "));
    if((elem->isAuthors() || elem->isEditors() || elem->isYearOfPublication()) && elem->isCurrentPage()) { c.append(COMMA); }
    c.append(elem->getCurrentPage());
    c = c.simplified();

    if(!c.isEmpty()) { citation = c.prepend("(").append(")"); }
    else { citation = ""; }
}


void ChicagoInTextStyle::scientificArticleBibliography()
{
    // issue number between paretheses
    QString nameBlock;
    {
        nameBlock.append(listBibliographyNames(elem->getAuthorsList()));
        nameBlock.append(elem->getYearOfPublication(" ", "."));
        nameBlock.append(elem->getTitle(" \"", "\""));
        nameBlock.append(elem->getJournal(". " + FORM_OPEN, FORM_CLOSE));
    }

    QString secondBlock;
    {
        secondBlock.append(elem->getVolumNumber(" " + FORM_OPEN, FORM_CLOSE));
        if(elem->isMonthOfPublication()){ secondBlock.append(elem->getMonthOfPublication(" (", ")")); }
        else { secondBlock.append(elem->getIssueNumberJournal(" (", ")")); }

        if(elem->isMonthAndYearOfPublication() && elem->isPages()) { secondBlock.append(": "); }
        else if(!elem->isMonthAndYearOfPublication() && elem->isPages()) { secondBlock.append(". "); }

        secondBlock.append(elem->getPages());

        if(elem->isDOI()){
            secondBlock.append(elem->getDOI(". "));
        }
        else{
            secondBlock.append(elem->getWebSiteUrl(". "));
        }

        secondBlock.append(".");
    }

    bibliography = nameBlock + secondBlock;

}



void ChicagoInTextStyle::newspaperArticleCitation()
{
    QString c;

    if(elem->isAuthors()) {
        c.append(listCitationNames_familyNameOnly(elem->getAuthors(), "", "", 4));
    }
    else if(elem->isEditors() && !elem->isAuthors()){
        c.append(listCitationNames_familyNameOnly(elem->getEditors(), "", "", 4));
    }

    c.append(elem->getYearOfPublication(" "));
    if((elem->isAuthors() || elem->isEditors() || elem->isYearOfPublication()) && elem->isCurrentPage()) { c.append(COMMA); }
    c.append(elem->getCurrentPage());
    c = c.simplified();

    if(!c.isEmpty()) { citation = c.prepend("(").append(")"); }
    else { citation = ""; }
}

void ChicagoInTextStyle::newspaperArticleBibliography()
{
    QString nameBlock = "";
    {
        nameBlock.append(listBibliographyNames(elem->getAuthorsList()));
        nameBlock.append(elem->getYearOfPublication(" ", "."));
        nameBlock.append(elem->getTitle(" \"", "\""));
        nameBlock.append(elem->getJournal(". " + FORM_OPEN, FORM_CLOSE));
    }

    QString secondBlock = "";
    {
        secondBlock.append(elem->getMonthAndDayOfPublication(". "));
        secondBlock.append(elem->getWebSiteUrl(". "));

        secondBlock.append(".");
    }
    bibliography = nameBlock + secondBlock;
}




void ChicagoInTextStyle::dissertationCitation()
{
    QString c;

    c.append(listCitationNames_familyNameOnly(elem->getAuthors(), "", "", 4));
    c.append(elem->getYearOfPublication(" "));
    c.append(elem->getPages(COMMA));
    c = c.simplified();

    citation = c.prepend("(").append(")");
}

void ChicagoInTextStyle::dissertationBibliography()
{
    QString first;
    {
        first.append(listBibliographyNames(elem->getAuthorsList()));
        first.append(elem->getYearOfPublication(" ", "."));
        first.append(elem->getTitle(" \"", "\"."));
    }

    QString second = " ";
    {
        second.append(elem->getDissertationType());
        if(elem->isDissertationType()) { second.append(COMMA); }
        second.append(elem->getInstitution());
        second.append(elem->getWebSiteUrl(". "));
    }

    bibliography = QString(first + second + ".").simplified();
}


void ChicagoInTextStyle::webSiteCitation()
{
    QString c;

    c.append(listCitationNames_familyNameOnly(elem->getAuthors(), "", "", 4));
    c.append(elem->getYearOfPublication(" "));
    c.append(elem->getPages(COMMA));
    c = c.simplified();

    citation = c.prepend("(").append(")");
}

void ChicagoInTextStyle::webSiteBibliography()
{
    QString firstBlock = "";
    {
        firstBlock.append(listBibliographyNames(elem->getAuthorsList()));
        firstBlock.append(elem->getYearOfPublication(" ", "."));
        firstBlock.append(elem->getTitle(" \"", "\""));
    }

    QString secondBlock = "";
    {
        secondBlock.append(elem->getWebSiteName(". " + FORM_OPEN, FORM_CLOSE));
        secondBlock.append(elem->getMonthAndDayOfPublication(". "));
        secondBlock.append(elem->getWebSiteUrl(". "));
        secondBlock.append(elem->getAccessDate(". (" + ACCESSED_CIT, ")"));
    }

    bibliography = firstBlock + secondBlock + ".";
}



QString ChicagoInTextStyle::listCitationNames_familyNameOnly(QList<NameComponents> lst, QString pre, QString post, int maximumNumberOfNames) const
{
    if(lst.isEmpty()) { return (""); }
    QString name;

    int i = 0;
    while( i < lst.length() ){
        if(i == maximumNumberOfNames) { break; }

        if(i == lst.length()-1 && 1 < lst.size()){
            name.append(AND);
        }
        else if(i != 0) { name.append(COMMA); }

        name.append(DocumentInfo::toTitleCase(singleCitationName_familyNameOnly(lst[i])));

        i++;
    }

    return name.prepend(pre).append(post);
}


QString ChicagoInTextStyle::singleCitationName_familyNameOnly(NameComponents n, QString pre, QString post) const
{
    if(!n.isValid()) { return QString(""); }

    if(n.isFamilyName()) { return n.getFamilyName().prepend(pre).append(post); }
    else if(n.isFirstName()) { return n.getFirstName().prepend(pre).append(post); }
    else if(n.isMiddleName()) { return n.getMiddleName().prepend(pre).append(post); }
    else { return QString(""); }
}



