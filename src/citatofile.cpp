#include "citatofile.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>

CitatoFile::CitatoFile(DocumentInfo *docInfo, QWidget *p)
{
    parent = new QWidget(p);
    documentInfo = new DocumentInfo();
    documentInfo = docInfo;

    fromDocumentInfoToJsonDoc();
}

CitatoFile::CitatoFile(QJsonDocument *jsDoc, QWidget *p)
{
    parent = new QWidget(p);
    jsonDocument = new QJsonDocument;
    jsonDocument = jsDoc;

    fromJsonDocumentToDocumentInfo();
}

CitatoFile::CitatoFile(const CitatoFile &other)
{
    parent = new QWidget(other.parent);
    jsonDocument = new QJsonDocument(*other.jsonDocument);
    documentInfo = new DocumentInfo(*other.documentInfo);
}

/**********************************************/

DocumentInfo CitatoFile::fromJsonDocumentToDocumentInfo(QJsonDocument *js)
{
//    QJsonObject *meta = new QJsonObject(js->object().value("__META__").toObject());
    QJsonObject *d = new QJsonObject(js->object().value("Information").toObject());

    DocumentInfo *doc = new DocumentInfo();
    doc->setDocumentLanguage(d->value("_Language Int").toInt());
    doc->setDocumentType(d->value("_Document Type Int").toInt());

    doc->setAuthors(fromArrayToListOfNames(d->value("Authors").toArray()));
    doc->setTitle(d->value("Title").toString());
    doc->setDissertationType(d->value("Dissertation Type").toInt());
    doc->setInstitution(d->value("Institution").toString());
    doc->setChapter(d->value("Chapter").toString());
    doc->setJournal(d->value("Journal").toString());
    doc->setIssueNumberJournal(d->value("Issue Number").toString());
    doc->setEditors(fromArrayToListOfNames(d->value("Editors").toArray()));
    doc->setTranslators(fromArrayToListOfNames(d->value("Translators").toArray()));
    doc->setEditionNumber(d->value("Edition Number").toString());
    doc->setEditionsHome(d->value("Editions Home").toString());
    doc->setVolumNumber(d->value("Volum Number").toString());
    doc->setVolumTitle(d->value("Volum Title").toString());
    doc->setLocation(d->value("Location").toString());
    doc->setYearOfPublication(d->value("Year of Publication").toString());
    doc->setMonthOfPublication(d->value("Month of Publication").toString());
    doc->setDayOfPublication(d->value("Day of Publication").toString());
    doc->setCurrentPage(d->value("Current Page").toString());
    doc->setPageFrom(d->value("Page From").toString());
    doc->setPageTo(d->value("Page To").toString());
    doc->setWebSiteName(d->value("Web Site's Name").toString());
    doc->setWebSiteUrl(d->value("Web Site's URL").toString());
    doc->setAccessYear(d->value("Access Date: Year").toString());
    doc->setAccessMonth(d->value("Access Date: Month").toString());
    doc->setAccessDay(d->value("Access Date: Day").toString());
    doc->setDOI(d->value("DOI").toString());
    doc->setISBN(d->value("ISBN").toString());

    delete d;

    return *doc;
}

void CitatoFile::fromJsonDocumentToDocumentInfo()
{
    documentInfo = new DocumentInfo(fromJsonDocumentToDocumentInfo(jsonDocument));
}

QJsonDocument CitatoFile::fromDocumentInfoToJsonDoc(DocumentInfo *docInfo)
{
    QJsonObject *meta = new QJsonObject;
    meta->insert("filetype", "citato file");
    meta->insert("extension", ".citato");
    meta->insert("program", "Citato");
    meta->insert("program version", "");

    QJsonObject *information = new QJsonObject;

    information->insert("_Document Type String", docInfo->getDocumentTypeStr());
    information->insert("_Document Type Int", docInfo->getDocumentTypeInt());
    information->insert("_Language String", docInfo->getDocumentLanguageStr());
    information->insert("_Language Int", docInfo->getDocumentLanguageInt());
    information->insert("_comment", "");

    information->insert("Authors", QJsonValue(fromListOfNamesToArray(docInfo->getAuthorsList())));
    information->insert("Title", docInfo->getTitle());
    information->insert("Dissertation Type Int", docInfo->getDissertationTypeInt());
    information->insert("Dissertation Type Str", docInfo->getDissertationType());
    information->insert("Institution", docInfo->getInstitution());
    information->insert("Chapter", docInfo->getChapter());
    information->insert("Journal", docInfo->getJournal());
    information->insert("Issue Number", docInfo->getIssueNumberJournal());
    information->insert("Editors", QJsonValue(fromListOfNamesToArray(docInfo->getEditorsList())));
    information->insert("Translators", QJsonValue(fromListOfNamesToArray(docInfo->getTranslatorsList())));
    information->insert("Edition Number", docInfo->getEditionNumber());
    information->insert("Editions Home", docInfo->getEditionsHome());
    information->insert("Volum Number", docInfo->getVolumNumber());
    information->insert("Volum Title", docInfo->getVolumTitle());
    information->insert("Location", docInfo->getLocation());
    information->insert("Year of Publication", docInfo->getYearOfPublication());
    information->insert("Month of Publication", docInfo->getMonthOfPublication());
    information->insert("Day of Publication", docInfo->getDayOfPublication());
    information->insert("Current Page", docInfo->getCurrentPage());
    information->insert("Page From", docInfo->getPageFrom());
    information->insert("Page To", docInfo->getPageTo());
    information->insert("Web Site's Name", docInfo->getWebSiteName());
    information->insert("Web Site's URL", docInfo->getWebSiteUrl());
    information->insert("Access Date: Year", docInfo->getAccessYear());
    information->insert("Access Date: Month", docInfo->getAccessMonth());
    information->insert("Access Date: Day", docInfo->getAccessDay());
    information->insert("DOI", docInfo->getDOI());
    information->insert("ISBN", docInfo->getISBN());

    QJsonObject js;
    js.insert("__META__", QJsonValue(*meta));
    js.insert("Information", QJsonValue(*information));

    QJsonDocument jsDoc;
    jsDoc.setObject(js);

    delete meta;
    delete information;

    return jsDoc;
}

void CitatoFile::fromDocumentInfoToJsonDoc()
{
    jsonDocument = new QJsonDocument(fromDocumentInfoToJsonDoc(documentInfo));
}


void CitatoFile::saveToFile(QJsonDocument *js)
{
    QString filename = QFileDialog::getSaveFileName(nullptr, "Save Citato Document", QDir::homePath(), "citato files (*.citato)");
    if(filename.isEmpty()) { return; }

    QFile f(filename);
    if(!f.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QMessageBox::critical(nullptr, "Error",
                              "An error has occured. The file cannot be opened.");
        return;
    }

    f.write(js->toJson());
    f.close();
}

void CitatoFile::saveToFile()
{
    saveToFile(jsonDocument);
}


QJsonDocument CitatoFile::loadFromFile()
{
    QString filename = QFileDialog::getOpenFileName(nullptr, "Open Document", QDir::homePath());
    if(filename.isEmpty()) { return QJsonDocument(); }

    QFile f(filename);
    if(!f.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox::critical(nullptr, "Error",
                              "An error has occured. The file cannot be opened.");
        return QJsonDocument();
    }

    QJsonDocument *js = new QJsonDocument;
    js->setObject(QJsonDocument::fromJson(f.readAll()).object());
    f.close();

    if(isValid(js)) { return *js; }
    else{
        QMessageBox::critical(nullptr, "Not Valid",
                              "This file is not a valid Citato document");
        return QJsonDocument();

    }
}


QJsonArray CitatoFile::fromNameComponentsToArray(NameComponents name)
{
    QJsonArray nameArray;
    nameArray.append(QJsonValue(name.getFirstName()));
    nameArray.append(QJsonValue(name.getMiddleName()));
    nameArray.append(QJsonValue(name.getFamilyName()));
    return nameArray;
}


NameComponents CitatoFile::fromArrayToNameComponents(QJsonArray nameArray)
{
    NameComponents n;
    n.setFirstName(nameArray.at(0).toString());
    n.setMiddleName(nameArray.at(1).toString());
    n.setFamilyName(nameArray.at(2).toString());
    return n;
}


QJsonArray CitatoFile::fromListOfNamesToArray(QList<NameComponents> names)
{	// additional names will be in an array of arrays.
    QJsonArray jsArray;

    for(int i = 0 ; i < names.size() ; i++){
        QJsonArray singleName;
        singleName.append(QJsonValue(names[i].getFirstName()));
        singleName.append(QJsonValue(names[i].getMiddleName()));
        singleName.append(QJsonValue(names[i].getFamilyName()));
        jsArray.append(QJsonValue(singleName));
    }

    return jsArray;
}

QList<NameComponents> CitatoFile::fromArrayToListOfNames(QJsonArray jsArray)
{
    QList<NameComponents> names;
    for(int i = 0 ; i < jsArray.size() ; i++){
        NameComponents n;
        QJsonArray single = jsArray.at(i).toArray();
        n.setFirstName(single.at(0).toString());
        n.setMiddleName(single.at(1).toString());
        n.setFamilyName(single.at(2).toString());

        names.push_back(n);
    }

    return names;
}


QTreeWidgetItem CitatoFile::fromJsonDocumentToTreeWidgetItem(QJsonObject *obj)
{
    QTreeWidgetItem wt;
    QJsonObject inf = obj->value("Information").toObject();

    NameComponents auth = NameComponents(inf.value("Author's Name, Primary").toArray().at(0).toString(),
                                         inf.value("Author's Name, Primary").toArray().at(1).toString(),
                                         inf.value("Author's Name, Primary").toArray().at(2).toString());

    wt.setText(0, auth.getCitation());
    wt.setText(1, inf.value("Title").toString());
//    wt.setText(2, inf.value(""));
//    wt.setText(3, inf.value(""));
//    wt.setText(4, inf.value(""));

    return wt;
}


QTreeWidgetItem CitatoFile::fromJsonDocumentToTreeWidgetItem(QJsonDocument *doc)
{
    QJsonObject obj = doc->object().value("Information").toObject();
    return fromJsonDocumentToTreeWidgetItem(&obj);
}

QJsonDocument CitatoFile::fromFileToJsonDocument(QFile *f)
{
    if(!f->exists()) { return QJsonDocument(); }
    if(!f->isOpen()){
        if(!f->open(QIODevice::ReadOnly)){ return QJsonDocument(); }
    }
    QJsonDocument d = QJsonDocument::fromJson(f->readAll());
    f->close();

    return d;
}

QJsonObject CitatoFile::fromFileToJsonObject(QFile *f)
{
    if(!f->exists()) { return QJsonObject(); }
    if(!f->isOpen()){
        if(!f->open(QIODevice::ReadOnly)){ return QJsonObject(); }
    }
    QJsonObject d = QJsonObject(QJsonDocument::fromJson(f->readAll()).object());
    f->close();

    return d;
}

bool CitatoFile::isValid()
{
    if(!jsonDocument) { return false; }
    else{ return isValid(jsonDocument); }
}

bool CitatoFile::isValid(QJsonDocument *j)
{
    if(!j) { return false; }
    std::unique_ptr<QJsonObject> obj(new QJsonObject(j->object()));

    return isValid(obj.release());
}

bool CitatoFile::isValid(const QJsonObject *o)
{
    if(!o) { return false; }

    if(!o->contains("__META__") || !o->contains("Information")) { return false; }
    std::unique_ptr<QJsonObject> m(new QJsonObject(o->value("__META__").toObject()));
    std::unique_ptr<QJsonObject> i(new QJsonObject(o->value("Information").toObject()));
    QJsonObject *meta = m.get();
    QJsonObject *inf = i.get();

    if(!meta->contains("program")) { return false; }
    if(meta->value("program") != "Citato") { return false; }
    if(!meta->contains("filetype")) { return false; }
    if(meta->value("filetype") != "citato file") { return false; }
    if(!inf->contains("_Language Int")) { return false; }
    if(!inf->contains("_Document Type Int")) { return false; }
    if(!inf->contains("Dissertation Type Int")) { return false; }
    if(!inf->contains("Title")) { return false; }
    if(!inf->contains("Chapter")) { return false; }
    if(!inf->contains("DOI")) { return false; }

    return true;
}

bool CitatoFile::isValidWithWarning(QJsonDocument *j)
{
    if(!isValid(j)){
        QMessageBox::warning(nullptr, "Note Valid Document",
                             "This is not a valid Citato document");
        return false;
    }
    return true;
}

bool CitatoFile::isValidWithWarning(QFile *f)
{
    QString path = QFileInfo(*f).absoluteFilePath();
    if(!f->open(QIODevice::ReadOnly)){
        QMessageBox::warning(nullptr, "Invalid file",
                             "This file cannot be opened.");
        return false;
    }
    QJsonObject *obj = new QJsonObject(QJsonDocument::fromJson(f->readAll()).object());

    if(isValid(obj)){
        delete obj;
        return true;
    }
    else{
        QString warn = QString(QString("the file ") + path + QString(" is not a valid citato document.")).simplified();
        QMessageBox::warning(nullptr, "Invalid file", warn);
        delete obj;

        return false;
    }
}




