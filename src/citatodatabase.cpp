#include "citatodatabase.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QFile>
#include <memory>
CitatoDatabase::CitatoDatabase(QWidget *p)
{
    database = new QJsonDocument;
    parent = new QWidget(p);
}

CitatoDatabase::CitatoDatabase(QJsonDocument db, QWidget *p)
{
    database = new QJsonDocument(db);
    parent = new QWidget(p);
}

CitatoDatabase::CitatoDatabase(QJsonDocument *db, QWidget *p)
{
    database = new QJsonDocument(*db);
    parent = new QWidget(p);
}

CitatoDatabase::CitatoDatabase(const CitatoDatabase &other)
{
    database = new QJsonDocument;
    database = other.database;
}

QJsonDocument CitatoDatabase::createNewDatabase()
{
    QJsonObject js;

    QJsonObject metadb;
    metadb.insert("name", "");
    metadb.insert("filetype", "citato database");
    metadb.insert("extension", ".citatodb");
    metadb.insert("program", "Citato");
    metadb.insert("program version", "");

    js.insert("__META__", QJsonValue(metadb));
    js.insert("Citato Documents", QJsonValue(QJsonArray()));

    return QJsonDocument(js);
}


QString CitatoDatabase::createAndSaveNewDatabaseToFile()
{
    QString filename = QFileDialog::getSaveFileName(nullptr, "Save Citato Database",
                                 QDir::homePath());
    if(filename.isEmpty()) { return QString(); }

    QFile f(filename);
    if(!f.open(QIODevice::WriteOnly | QIODevice::Text)){
        QMessageBox::critical(nullptr, "Fatal Error",
                              "An Error has occured while saving the database");
    }

    f.write(createNewDatabase().toJson());
    f.close();
    return filename;
}


QTreeWidgetItem CitatoDatabase::fromJsonDocumentToTopLevelTreeWidgetItem(QJsonDocument *doc)
{
    QTreeWidgetItem children;
    QJsonArray *array = new QJsonArray(doc->object().value("Citato Documents").toArray());

    for(int i = 0 ; i < array->size() ; i++){
        QJsonObject *obj = new QJsonObject(array->at(i).toObject());
        QTreeWidgetItem *child = new QTreeWidgetItem(CitatoFile::fromJsonDocumentToTreeWidgetItem(obj));

        children.addChild(child);
        delete obj;
    }

    return children;
}


QTreeWidgetItem CitatoDatabase::fromFileToTopLevelTreeWidget(QFile *f)
{
    if(!f->open(QIODevice::ReadOnly)){
        QMessageBox::warning(nullptr, "Not Valid File", "this file is not a valid citato database document");
        return QTreeWidgetItem();
    }

    std::unique_ptr<QJsonDocument> js(new QJsonDocument(QJsonDocument::fromJson(f->readAll())));
    return fromJsonDocumentToTopLevelTreeWidgetItem(js.release());
}

void CitatoDatabase::append(QJsonObject *source, QJsonDocument *&dest)
{
    dest->object().value("Citato Documents").toArray().append(*source);
}

void CitatoDatabase::append(CitatoFile *source, QJsonDocument *&dest)
{
    dest->object().value("Citato Documents").toArray().append(QJsonValue(source->getJsonDocument()->object()));
}

void CitatoDatabase::append(QJsonObject *source)
{
    database->object().value("Citato Documents").toArray().append(QJsonValue(*source));
}


bool CitatoDatabase::isValid(const QJsonDocument *js)
{
    if(js->isEmpty()) { return false; }

    QJsonObject o = js->object();
    if(!o.contains("__META__")) { return false; }

    QJsonObject meta = o.value("__META__").toObject();
    if(!meta.contains("filetype")) { return false; }
    if(meta.value("filetype") != "citato database") { return false; }
    if(!meta.contains("program")) { return false; }
    if(meta.value("program") != "Citato") { return false; };
    if(!meta.contains("extension")) { return false; }

    if(!o.contains("Citato Documents")) { return false; }
    if(!o.value("Citato Documents").isArray()) { return false; }

    return true;
}


bool CitatoDatabase::isValid()
{
    return isValid(database);
}



















