#ifndef CITATODATABASE_H
#define CITATODATABASE_H

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QTreeWidgetItem>
#include <QList>

#include "citatofile.h"

/* STRUCTURE OF A CITATO DATABASE DOCUMENT
 * 		An object which contains:
 * 			"__META__" an object that discribes the document
 * 			"array" an array with multiple documents (which are JsonObjects)
 */

class CitatoDatabase
{
public:
    CitatoDatabase(QWidget *p=Q_NULLPTR);
    CitatoDatabase(QJsonDocument db, QWidget *p=Q_NULLPTR);
    CitatoDatabase(QJsonDocument *db, QWidget *p=Q_NULLPTR);
    CitatoDatabase(const CitatoDatabase &other);

    inline void setParent(QWidget *p=Q_NULLPTR) { parent = p; }
    inline void setDatabase(QJsonDocument *d) { database = d; }
    inline QJsonDocument getJsonDocument() { return *database; }
    inline QJsonDocument* getJsonDocumentPtr() { return database; }
    inline QTreeWidgetItem toTopLevelTreeWidgetItem(){ return fromJsonDocumentToTopLevelTreeWidgetItem(database); }
    inline QTreeWidgetItem toTreeWidgetItem(){ return fromJsonDocumentToTopLevelTreeWidgetItem(database); }
    void append(QJsonObject *source);
    bool isValid();
    static QJsonDocument createNewDatabase();
    static QString createAndSaveNewDatabaseToFile();	// returns the filepath of the saved database
    static void append(CitatoFile *source, QJsonDocument *&dest);
    static void append(QJsonObject *source, QJsonDocument *&dest);
    static QTreeWidgetItem fromJsonDocumentToTopLevelTreeWidgetItem(QJsonDocument *doc);
    static QTreeWidgetItem fromFileToTopLevelTreeWidget(QFile *f);
    static bool isValid(const QJsonDocument *js);

private:
    QJsonDocument *database;
    QWidget *parent;
};

#endif // CITATODATABASE_H
