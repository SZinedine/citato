#ifndef PARTICLES_H
#define PARTICLES_H


// defining particles to use for each language
#define AR_COMMA "، "
#define EN_COMMA ", "

#define AR_ED_CIT "تحرير"
#define AR_ED_BIB "تحرير"
#define EN_ED_CIT "ed."
#define EN_ED_BIB "Edited by"
#define FR_ED_CIT "ed."
#define FR_ED_BIB "Édité par"

#define EN_EDNUM "th ed."
#define FR_EDNUM "e ed."
#define AR_EDNUM "الطبعة "

#define EN_VOLNUM "vol. "
#define FR_VOLNUM "vol. "
#define AR_VOLNUM "المجلد "

#define EN_NUM "no. "
#define FR_NUM "no. "
#define AR_NUM "رقم "

#define EN_TRANS_CIT "trans. "
#define EN_TRANS_BIB "Translated by "
#define FR_TRANS_CIT "trad. "
#define FR_TRANS_BIB "Traduit par "
#define AR_TRANS_CIT "ترجمة "
#define AR_TRANS_BIB "ترجم من طرف "

#define EN_ACCESSED_CIT "accessed "
#define EN_ACCESSED_BIB "accessed "
#define FR_ACCESSED_CIT "consulté le "
#define FR_ACCESSED_BIB "consulté le "
#define AR_ACCESSED_CIT "تاريخ التصفح "
#define AR_ACCESSED_BIB "تاريخ التصفح "

#define EN_OTHERS_CIT " et al."
#define FR_OTHERS_CIT " et al."
#define AR_OTHERS_CIT " وآخرون"

#define AR_AND " و"
#define EN_AND " and "
#define FR_AND " et "

#define AR_IN " في "
#define EN_IN " in "
#define FR_IN " dans "

#define FORM_OPEN_I "<i>"			// form for titles / revues. we could adapt this for arabic
#define FORM_CLOSE_I "</i>"
#define FORM_OPEN_B "<strong>"
#define FORM_CLOSE_B "</strong>"
#define FORM_OPEN_U "<u>"
#define FORM_CLOSE_U "</u>"

#endif // PARTICLES_H
