#ifndef DOCUMENTINFO_H
#define DOCUMENTINFO_H

#include <QString>
#include <QList>
#include "namecomponents.h"

/* the objective of creating this class
 * is to simplify storage, retriving,
 * and verification of the document's
 * information provided by the user
 *
 *  SETTERS
 * 		before initializing an element (like author)
 * 		this class treats the user input and remove spaces
 *
 * 	GETTERS
 * 		when calling a getter, if the element exists, return it
 * 		otherwise, return an empty string
 * 		the getters offer the possibility to append and prepend
 */


class DocumentInfo
{
public:
    DocumentInfo();
    DocumentInfo(QString languageString);
    DocumentInfo(int languageIndex);
    DocumentInfo(const DocumentInfo &other);

    void setDocumentLanguage(const QString languageString);
    void setDocumentLanguage(const int languageIndex);
    inline void setDocumentType(const int documentIndex) { documentType = documentIndex; }
    // setters
    inline void setAuthors(QList<NameComponents> auths) { authors = auths; }
    inline void setTitle(QString tit) { title = tit.simplified(); }
    void setDissertationType(int dissIndex);
    inline void setInstitution(QString inst) { institution = inst.simplified(); }
    inline void setChapter(QString chap) { chapter = chap.simplified(); }
    inline void setJournal(QString jour) { journal = jour.simplified(); }
    inline void setIssueNumberJournal(QString issueNb) { issueNumberJournal = issueNb.simplified(); }
    inline void setEditors(QList<NameComponents> eds) { editors = eds; }
    inline void setTranslators(QList<NameComponents> trans) { translators = trans; }
    inline void setEditionNumber(QString edn) { editionNumber = edn.simplified(); }
    inline void setEditionsHome(QString edh) { editionsHome = edh.simplified(); }
    inline void setVolumNumber(QString voln) { volumNumber = voln.simplified(); }
    inline void setVolumTitle(QString volt) { volumTitle = volt.simplified(); }
    inline void setLocation(QString loc) { location = loc.simplified(); }
    inline void setYearOfPublication(QString y) { yearOfPublication = y.simplified(); }
    inline void setMonthOfPublication(QString m) { monthOfPublication = m.simplified(); }
    inline void setDayOfPublication(QString d) { dayOfPublication = d.simplified(); }
    inline void setCurrentPage(QString y) { currentPage = y.simplified(); }
    inline void setPageFrom(QString pageF) { pageFrom = pageF.simplified();}
    inline void setPageTo(QString pageT) { pageTo = pageT.simplified(); }
    inline void setWebSiteName(QString wsn) { webSiteName = wsn.simplified(); }
    void setWebSiteUrl(QString ws);
    inline void setAccessYear(QString y) { accessYear = y.simplified(); }
    inline void setAccessMonth(QString m) { accessMonth = m.simplified(); }
    inline void setAccessDay(QString d) { accessDay = d.simplified(); }
    inline void setDOI(QString d) { DOI = d.simplified(); }
    inline void setISBN(QString isbn) { ISBN = isbn.simplified(); }

    QString getDocumentTypeStr() const;
    inline int getDocumentTypeInt() const { return documentType; }
    inline int getDocumentLanguageInt() const { return documentLanguageInt; }
    inline QString getDocumentLanguageStr() const { return documentLanguage; }

    // getters
    QList<NameComponents> getAuthors();
    QList<NameComponents> getAuthorsList();
    int getAuthorsListLength() const;
    QString getTitle(QString pre="", QString post="");
    QString getDissertationType(QString pre="", QString post="");
    int getDissertationTypeInt() const;
    QString getInstitution(QString pre="", QString post="");
    QString getChapter(QString pre="", QString post="");
    QString getJournal(QString pre="", QString post="");
    QString getIssueNumberJournal(QString pre="", QString post="");
    QList<NameComponents> getEditors() const;
    QList<NameComponents> getEditorsList() const;
    int getEditorsListLength() const ;
    QList<NameComponents> getTranslator() const;
    QList<NameComponents> getTranslatorsList() const;
    int getTranslatorsListLength() const;
    QString getEditionNumber(QString pre="", QString post="");
    QString getEditionsHome(QString pre="", QString post="");
    QString getVolumNumber(QString pre="", QString post="");
    QString getVolumTitle(QString pre="", QString post="");
    QString getLocation(QString pre="", QString post="");
    QString getYearOfPublication(QString pre="", QString post="");
    QString getMonthOfPublication(QString pre="", QString post="");
    QString getDayOfPublication(QString pre="", QString post="");
    QString getMonthAndYearOfPublication(QString pre="", QString post="");
    QString getMonthAndDayOfPublication(QString pre="", QString post="");
    QString getPublicationDate(QString pre="", QString post="");
    QString getCurrentPage(QString pre="", QString post="");
    QString getPageFrom(QString pre="", QString post="");
    QString getPageTo(QString pre="", QString post="");
    QString getPages(QString pre="", QString post="");
    QString getWebSiteName(QString pre="", QString post="");
    QString getWebSiteUrl(QString pre="", QString post="");
    QString getAccessDate(QString pre="", QString post="");
    QString getAccessYear(QString pre="", QString post="");
    QString getAccessMonth(QString pre="", QString post="");
    QString getAccessMonthAndYear(QString pre="", QString post="");
    QString getAccessDay(QString pre="", QString post="");
    QString getDOI(QString pre="", QString post="");
    QString getISBN(QString pre="", QString post="");

    //verificators
    inline bool isAuthor() const 			{ return isAuthors(); }
    inline bool isAuthors() const 			{ return !authors.isEmpty(); }
    inline bool isTitle() const 			{ return !title.isEmpty(); }
    inline bool isDissertationType() const 	{ return (0 < dissertationTypeInt) ? true : false; }
    inline bool isInstitution() const 		{ return !institution.isEmpty(); }
    inline bool isChapter() const 			{ return !chapter.isEmpty(); }
    inline bool isJournal() const 			{ return !journal.isEmpty(); }
    inline bool isIssurNumberJournal() const{ return !issueNumberJournal.isEmpty(); }
    inline bool isEditor() const 			{ return isEditors(); }
    inline bool isEditors() const 			{ return !editors.isEmpty(); }
    inline bool isTranslator() const 		{ return isTranslators(); }
    inline bool isTranslators() const 		{ return !translators.isEmpty(); }
    inline bool isEditionNumber() const 	{ return !editionNumber.isEmpty(); }
    inline bool isEditionsHome() const 		{ return !editionsHome.isEmpty(); }
    inline bool isVolumNumber() const 		{ return !volumNumber.isEmpty(); }
    inline bool isVolumTitle() const		{ return !volumTitle.isEmpty(); }
    inline bool isLocation() const 			{ return !location.isEmpty(); }
    inline bool isYearOfPublication() const	{ return !yearOfPublication.isEmpty(); }
    inline bool isMonthOfPublication() const{ return !monthOfPublication.isEmpty(); }
    inline bool isDayOfPublication() const	{ return !dayOfPublication.isEmpty(); }
    inline bool isPublicationDate() 	    { return !getPublicationDate().isEmpty(); }
    inline bool isMonthAndYearOfPublication(){return !getMonthAndYearOfPublication().simplified().isEmpty(); }
    inline bool isCurrentPage() const 		{ return !currentPage.isEmpty(); }
    inline bool isPageFrom() const 			{ return !pageFrom.isEmpty(); }
    inline bool isPageTo() const 			{ return !pageTo.isEmpty(); }
    inline bool isPages() 					{ return !getPages().simplified().isEmpty(); }
    inline bool isWebSiteName() const 		{ return !webSiteName.isEmpty(); }
    inline bool isWebSiteUrl() const 		{ return !webSiteUrl.isEmpty(); }
    inline bool isAccessYear() const		{ return !accessYear.isEmpty(); }
    inline bool isAccessMonth() const		{ return !accessMonth.isEmpty(); }
    inline bool isAccessDay() const			{ return !accessDay.isEmpty(); }
    inline bool isAccessMonthAndYear()      { return !getAccessMonthAndYear().isEmpty(); }
    inline bool isDOI() const 				{ return !DOI.isEmpty(); }
    inline bool isISBN() const 				{ return !ISBN.isEmpty(); }

    // other functions
    QString formatDate(QString d, QString m, QString y, QString pre="", QString post="");
    static QString toTitleCase(QString str);
    void clear();


private:
    QString documentLanguage = "";
    int documentLanguageInt = 0;
    int documentType = 0;

    QList<NameComponents> authors;			// if there are more than one author
    QString title = "";						// the title of the document. absolutely mandatory
    QString dissertationType = "";			// master dissertation, magister, phd...
    int dissertationTypeInt = 0;
    QString institution = "";				// if the document is published by an institution
    QString chapter = "";					// in the case of a chapter of a book
    QString journal = "";					// in case of a journal or newspaper
    QString issueNumberJournal = "";		// issue number for a journal or a magazine
    QList<NameComponents> editors;			// more than one editor
    QList<NameComponents> translators;		// more than one translator
    QString editionNumber = "";				// if there is more than one edition of the book
    QString editionsHome = "";
    QString volumNumber = "";				// multi-volum book
    QString volumTitle = "";
    QString location = "";					// the country/town where the work was published
    QString yearOfPublication = "";			// year of publication
    QString monthOfPublication = "";
    QString dayOfPublication = "";
    QString timePublication = "";
    QString currentPage = "";
    QString pageFrom = "";					// page from
    QString pageTo = "";					// page to
    QString webSiteName = "";
    QString webSiteUrl = "";
//    QString accessDate = "";
    QString accessYear = "";
    QString accessMonth = "";
    QString accessDay = "";
    QString DOI = "";
    QString ISBN = "";
};

#endif // DOCUMENTINFO_H
