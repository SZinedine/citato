#ifndef STYLECOMPOSER_H
#define STYLECOMPOSER_H

#include <QString>
#include <QList>
#include "namecomponents.h"
#include "documentinfo.h"

#include "particles.h"
#include "abstractstyle.h"

class ChicagoFootnoteStyle : public AbstractStyle
{
public:
    ChicagoFootnoteStyle(DocumentInfo &comp, int documentType, int lang, int format=1);
    ~ChicagoFootnoteStyle();

    QString listCitationNames(QList<NameComponents> lst, QString pre="", QString post="", int maximumNumberOfNames=10) const override;
    QString listBibliographyNames(QList<NameComponents> lst, QString pre="", QString post="", int maximumNumberOfNames=10) const override;
    QString singleCitationName(NameComponents n, QString pre="", QString post="") const override;
    QString singleBibliographyName(NameComponents n, QString pre="", QString post="") const override;

protected:
    void adaptLanguage();			// adapt particles language (like latin/arabic commas...)

    void bookCitation() override;							// book
    void bookBibliography() override;
    void chapterCitation() override;							// chapter of a book
    void chapterBibliography() override;
    void scientificArticleCitation() override;				// scientific article (from a journal)
    void scientificArticleBibliography() override;
    void newspaperArticleCitation() override;				// article from a newspaper
    void newspaperArticleBibliography() override;
    void dissertationCitation() override;					// dissertation
    void dissertationBibliography() override;
    void webSiteCitation() override;							// web site
    void webSiteBibliography() override;
};

#endif // STYLECOMPOSER_H


