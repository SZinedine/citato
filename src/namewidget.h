#ifndef NAMEWIDGET_H
#define NAMEWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QString>

#include "namecomponents.h"

// three QNameEdits designed to suit the management of names

class NameWidget  : public QWidget
{
    Q_OBJECT
public:

    enum Person : unsigned char{
        author = 0,
        editor,
        translator
    };

    explicit NameWidget(int lang, int Xth, Person person,
                        int labelMinimumWidth, QWidget *parent=Q_NULLPTR);
    NameComponents getName();
    void setName(NameComponents name);
    void clear();
    void adaptLanguage(int language);
    inline QHBoxLayout* getLayout() { return layout; }
    inline QLabel* getLabel() {return lab; }
    inline bool isValid() { return getName().isValid(); }

private:
    int n;			// the number which comes, in the label, after author/editor..
    QLabel *lab;
    Person per;		// type of person
    QHBoxLayout *layout;
    QLineEdit *firstName;
    QLineEdit *middleName;
    QLineEdit *familyName;
};

#endif // NAMEWIDGET_H
