#include "testdocument.h"
#include <QDebug>

TestDocument::TestDocument(DocumentInfo doc, const int typeDoc, int lang, const QWidget *parent)
{
    document = new DocumentInfo(doc);
    parent = parent;
    listOfMissingFields = "";
    errors = false;
    language = lang;

    switch(typeDoc){
    case 1:
        testBook();
        break;
    case 2:
        testChapter();
        break;
    case 3:
        testScientificArticle();
        break;
    case 4:
        testNewspaperArticle();
        break;
    case 5:
        testDissertation();
        break;
    case 6:
        testWebSite();
        break;
    }
}


void TestDocument::testBook()
{
    if(!document->isTitle()) { addTitle(); errors = true; }
    if(!document->isEditionsHome() && !document->isLocation()){
        addLocation();
        addEditionsHome();
        errors = true;
    }
}

void TestDocument::testChapter()
{
    if(!document->isTitle()) { addTitle(); errors = true; }
    if(!document->isChapter()) { addChapter(); errors = true; }
    if(!document->isEditionsHome() && !document->isLocation()){
        addLocation();
        addEditionsHome();
        errors = true;
    }
}


void TestDocument::testScientificArticle()
{
    if(!document->isTitle()) { addTitle(); errors = true; }
    if(!document->isJournal()) { addJournal(); errors = true; }
}


void TestDocument::testNewspaperArticle()
{
    if(!document->isTitle()) { addTitle(); errors = true; }
    if(!document->isJournal()) { addJournal(); errors = true; }
}


void TestDocument::testDissertation()
{
    if(!document->isTitle()) { addTitle(); errors = true; }
    if(!document->isDissertationType()) { addDissertationType(); errors = true; }
    if(!document->isInstitution()) { addInstitution(); errors = true; }

}

void TestDocument::testWebSite()
{
    if(!document->isWebSiteUrl()) { addWebSiteUrl(); errors = true; }
}

/**************************************/
/******** "Add" function members ******/
/**************************************/

void TestDocument::addTitle(){
    switch(language){
    case 1: listOfMissingFields.append(QString("<br>") + QString("Title").prepend(" - "));
        break;
    case 2:listOfMissingFields.append(QString("<br>") + QString("Titre").prepend(" - "));
        break;
    case 3: listOfMissingFields.append(QString("<br>") + QString("العنوان").prepend(" - "));
        break;
    }
}

void TestDocument::addChapter(){
    switch(language){
    case 1: listOfMissingFields.append(QString("<br>") + QString("Chapter Title").prepend(" - "));
        break;
    case 2: listOfMissingFields.append(QString("<br>") + QString("Titre du chapitre").prepend(" - "));
        break;
    case 3: listOfMissingFields.append(QString("<br>") + QString("عنوان الفصل").prepend(" - "));
        break;
    }
}

void TestDocument::addDissertationType(){
     switch(language){
    case 1: listOfMissingFields.append(QString("<br>") + QString("Dissesrtation type").prepend(" - "));
        break;
    case 2: listOfMissingFields.append(QString("<br>") + QString("Type de mémoire/thèse").prepend(" - "));
        break;
    case 3: listOfMissingFields.append(QString("<br>") + QString("نوع المذكرة").prepend(" - "));
        break;
    }
}

void TestDocument::addYear()
{
    switch(language){
    case 1: listOfMissingFields.append(QString("<br>") + QString("Year/time of publication").prepend(" - "));
        break;
    case 2: listOfMissingFields.append(QString("<br>") + QString("Année/date de publication").prepend(" - "));
        break;
    case 3: listOfMissingFields.append(QString("<br>") + QString("سنة أو تاريخ النشر").prepend(" - "));
        break;
    }
}

void TestDocument::addJournal(){
    switch(language){
    case 1: listOfMissingFields.append(QString("<br>") + QString("Journal/Newspaper").prepend(" - "));
        break;
    case 2: listOfMissingFields.append(QString("<br>") + QString("Revue/Journal").prepend(" - "));
        break;
    case 3: listOfMissingFields.append(QString("<br>") + QString("المجلة أو الجريدة").prepend(" - "));
        break;
    }
}

void TestDocument::addInstitution(){
    switch(language){
    case 1: listOfMissingFields.append(QString("<br>") + QString("Institution/university").prepend(" - "));
        break;
    case 2: listOfMissingFields.append(QString("<br>") + QString("Institution/Université").prepend(" - "));
        break;
    case 3: listOfMissingFields.append(QString("<br>") + QString("المؤسسة أو الجامعة").prepend(" - "));
        break;
    }
}

void TestDocument::addLocation(){
    switch(language){
    case 1: listOfMissingFields.append(QString("<br>") + QString("Country/City of publication").prepend(" - "));
        break;
    case 2: listOfMissingFields.append(QString("<br>") + QString("Pays/Ville de publication").prepend(" - "));
        break;
    case 3: listOfMissingFields.append(QString("<br>") + QString("بلد أو مدينة النشر").prepend(" - "));
        break;
    }
}

void TestDocument::addEditionsHome(){
    switch(language){
    case 1: listOfMissingFields.append(QString("<br>") + QString("Editions' Home / publisher").prepend(" - "));
        break;
    case 2: listOfMissingFields.append(QString("<br>") + QString("Maison d'édition").prepend(" - "));
        break;
    case 3: listOfMissingFields.append(QString("<br>") + QString("دار النشر").prepend(" - "));
        break;
    }
}

void TestDocument::addWebSiteUrl(){
    switch(language){
    case 1: listOfMissingFields.append(QString("<br>") + QString("Web Site URL").prepend(" - "));
        break;
    case 2: listOfMissingFields.append(QString("<br>") + QString("l'URL du site web").prepend(" - "));
        break;
    case 3: listOfMissingFields.append(QString("<br>") + QString("رابط الموقع").prepend(" - "));
        break;
    }
}



//// GETTER OF THE ERROR MESSAGE
QString TestDocument::getErrorMessage(){

    switch(language){
    case 1: errorMessage = "Important fields have not been filled:";
        break;
    case 2: errorMessage = "Les Champs essentiels suivants n'ont pas été remplis:";
        break;
    case 3: errorMessage = "لم يتم إدخال العناصر التالية:";
        break;
    }
    errors = false;

    return errorMessage + listOfMissingFields;
}


bool TestDocument::isError(){
    return errors;
}


TestDocument::~TestDocument(){
    delete document;
}









