#include "field.h"

Field::Field(QWidget *parent) : QWidget(parent)
{
    label = new QLabel;
    label->setMinimumWidth(110);
    label->setWordWrap(true);
    line = new QLineEdit;
    line->setAcceptDrops(false);


    layout = new QHBoxLayout;
    layout->addWidget(label);
    layout->addWidget(line);
    setLayout(layout);

    layout->setContentsMargins(0, 0, 0, 0);
    setContentsMargins(0, 0, 0, 0);
}


void Field::changeText(QString labelText, QString linePlaceHolder)
{
    label->setText(labelText);
    line->setPlaceholderText(linePlaceHolder);
}

void Field::setLabelMaximumWidth(int labMax)
{
    label->setMaximumWidth(labMax);
}

/*******************************************/
/******** PagesField Class *****************/
/*******************************************/

PagesField::PagesField(QWidget *parent) : Field(parent)
{
    toLine = new QLineEdit;
    toLine->setAcceptDrops(false);
    layout->addWidget(toLine);
}

void PagesField::changeText(QString labelText, QString fromLinePlaceHolder, QString toLineHolder)
{
    label->setText(labelText);
    line->setPlaceholderText(fromLinePlaceHolder);
    toLine->setPlaceholderText(toLineHolder);
}

void PagesField::setLineEditPlaceholders(QString from, QString to)
{
    line->setPlaceholderText(from);
    toLine->setPlaceholderText(to);
}

void PagesField::clear()
{
    line->clear();
    toLine->clear();
}

void PagesField::setLineEditText(QString from, QString to)
{
    line->setText(from);
    toLine->setText(to);
}

/*******************************************/
/******** DateField Class ******************/
/*******************************************/

DateField::DateField(QWidget *parent) : Field(parent)
{
    monthOrSeason = new QLineEdit;
    day = new QLineEdit;

    monthOrSeason->setAcceptDrops(false);
    day->setAcceptDrops(false);

    layout->addWidget(monthOrSeason);
    layout->addWidget(day);
}


void DateField::clear()
{
    line->clear();
    monthOrSeason->clear();
    day->clear();
}


void DateField::setLineEditText(const QString d, const QString m, const QString y)
{
    line->setText(y);
    monthOrSeason->setText(m);
    day->setText(d);
}


void DateField::setLineEditPlaceholders(const QString y, const QString m, const QString d)
{
    line->setPlaceholderText(y);
    monthOrSeason->setPlaceholderText(m);
    day->setPlaceholderText(d);
}

void DateField::changeText(const QString &labelText, const QString &y, const QString &m, const QString &d)
{
    label->setText(labelText);
    setLineEditPlaceholders(y, m, d);
}







