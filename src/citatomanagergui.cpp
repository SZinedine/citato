#include "citatomanagergui.h"
#include <QMessageBox>
#include <memory>
#include <QDebug>

CitatoManagerGui::CitatoManagerGui(QWidget *parent) : QMainWindow(parent)
{
    setWindowModality(Qt::WindowModal);
    setWindowTitle("Citato Manager");
    setFixedSize(700, 400);
    setupCentral();
    setupMenu();
    setupLayout();
    setupSignalsAndSlots();
}


//void CitatoManagerGui::loadDatabase()	// replacing it (deprecated)
//{

//    for(int i = 0 ; i < files.size() ; ++i){
//        // load the document and store it in a map along with its path
//        QFile f(files[i]);
//        f.open(QFile::ReadOnly);
//        QJsonDocument *db = new QJsonDocument(QJsonDocument::fromJson(f.readAll()));
//        dbItems.insert(files[i], db);

//        // show the item in the widget
//        QTreeWidgetItem *widgetItem = new QTreeWidgetItem(CitatoDatabase::fromJsonDocumentToTopLevelTreeWidgetItem(db));
//        widgetItem->setText(0, files[i]);
//        containerWidget->addTopLevelItem(widgetItem);
//    }
//}

void CitatoManagerGui::addItemToTreeWidget(QJsonDocument *db, const QString &filename)
{
    if(CitatoDatabase::isValid(db)){ return; }

    // convert jsonDocument to treeWidget item and store it a smart pointer
    std::unique_ptr<QTreeWidgetItem> tw(new QTreeWidgetItem(CitatoDatabase::fromJsonDocumentToTopLevelTreeWidgetItem(db)));

    tw.get()->setText(0, filename);	// make the first column of the db the filename to easily retrieve it later
    dbItems.insert(filename, db);	// keep track on which item was added to the TreeWidget

    containerWidget->addTopLevelItem(tw.release());
}

void CitatoManagerGui::addItemToTreeWidget(QFile *dbFile)
{
    if(!dbFile->open(QIODevice::ReadOnly)) { return; }
    QJsonDocument *js = new QJsonDocument(QJsonDocument::fromJson(dbFile->readAll()));

    addItemToTreeWidget(js, QFileInfo(*dbFile).absoluteFilePath());

    dbFile->close();
}

void CitatoManagerGui::openDatabase()
{
    QStringList files = QFileDialog::getOpenFileNames(this, "Open Citato Databases", QDir::homePath());
    if(files.isEmpty()){ return;}

    for(int i = 0 ; i < files.size() ; ++i){
        QFile *f = new QFile(files[i]);
        addItemToTreeWidget(f);
    }
}

// create a new database and asks the user if he wants to load it to citato manager
void CitatoManagerGui::createNewDatabase()
{
    QString filename = CitatoDatabase::createAndSaveNewDatabaseToFile();
    if(!QFile(filename).exists()){ QMessageBox::critical(nullptr, "Error", "Error. The file hasn't been created."); return; }

    QMessageBox question;
    question.setText("The database has been created.");
    question.setInformativeText("Do you want to open the created database ?");
    question.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    question.setDefaultButton(QMessageBox::Yes);
    int i = question.exec();
    qDebug() << "after creating databse. int result: " << i;


//    if(answer == QMessageBox::Yes){
//        std::unique_ptr<QFile> it(new QFile(filename));
//        it.get()->open(filename);


//        CitatoDatabase::fromFileToTopLevelTreeWidget(it.get());
//    }
}




/************************************************************/
/************* constructor abstraction **********************/
/************************************************************/
void CitatoManagerGui::setupCentral()
{
    mainWidget = new QWidget(this);
    mainLayout = new QGridLayout;
    mainWidget->setLayout(mainLayout);
    setCentralWidget(mainWidget);

    containerWidget = new QTreeWidget(this);
    containerWidget->setAlternatingRowColors(true);
    containerWidget->setAnimated(true);

    QStringList containerWidgetHeader;
    containerWidgetHeader << "Name" << "Title" << "Type";
    containerWidget->setHeaderLabels(containerWidgetHeader);

    importDatabaseButton = new QPushButton("Import Database", this);
    exportDatabaseButton = new QPushButton("Export Database", this);

    createNewDatabaseButton = new QPushButton("Create New Databse", this);

    addFileToDatabaseButton = new QPushButton(this);
    addFileToDatabaseButton->setIcon(QIcon(":/images/addItem.png"));
    addFileToDatabaseButton->setFixedWidth(40);

    removeItemFromDatabaseButton = new QPushButton(this);
    removeItemFromDatabaseButton->setIcon(QIcon(":/images/deleteItem.png"));
    removeItemFromDatabaseButton->setFixedWidth(40);

    quitButton = new QPushButton("Quit", this);
    quitButton->setIcon(QIcon(":/images/exit.png"));
}


void CitatoManagerGui::setupMenu()
{

}

void CitatoManagerGui::setupLayout()
{
    QVBoxLayout *importExportDatabaseLayout = new QVBoxLayout;
    importExportDatabaseLayout->addWidget(importDatabaseButton);
    importExportDatabaseLayout->addWidget(exportDatabaseButton);
    importExportDatabaseLayout->addWidget(createNewDatabaseButton);

    QHBoxLayout *loadRemoveItemLayout = new QHBoxLayout;
    loadRemoveItemLayout->addWidget(addFileToDatabaseButton);
    loadRemoveItemLayout->addWidget(removeItemFromDatabaseButton);

    mainLayout->addWidget(containerWidget, 1, 0, 5, 5);
    mainLayout->addLayout(importExportDatabaseLayout, 1, 5);
    mainLayout->addLayout(loadRemoveItemLayout, 0, 4);
    mainLayout->addWidget(quitButton, 5, 5);
}


void CitatoManagerGui::setupSignalsAndSlots()
{
    connect(quitButton, SIGNAL(clicked(bool)), this, SLOT(close()));
    connect(createNewDatabaseButton, SIGNAL(clicked(bool)),
            this, SLOT(createNewDatabase()));
    connect(importDatabaseButton, SIGNAL(clicked(bool)),
            this, SLOT(openDatabase()));
//    connect(addFileToDatabaseButton, SIGNAL(clicked(bool)), this, SIGNAL(addFileToDatabase()));
}
