#include "chicagofootnotestyle.h"
#include <iostream>
#include <QDebug>

ChicagoFootnoteStyle::ChicagoFootnoteStyle(DocumentInfo &comp, int documentType, int lang, int format)
    : AbstractStyle(comp, documentType, lang, format)
{
    adaptLanguage();
    callAppropriateDocument();
}

void ChicagoFootnoteStyle::adaptLanguage()
{
    // initialize particles according to the selected language

    switch(language){

    case 1:
        COMMA = EN_COMMA;
        IN = EN_IN;
        ED_BIB = EN_ED_BIB;
        ED_CIT = EN_ED_CIT;
        EDNUM = EN_EDNUM;
        VOLNUM = EN_VOLNUM;
        NUM = EN_NUM;
        AND = EN_AND;
        TRANS_CIT = EN_TRANS_CIT;
        TRANS_BIB = EN_TRANS_BIB;
        OTHERS_CIT = EN_OTHERS_CIT;
        ACCESSED_CIT = EN_ACCESSED_CIT;
        ACCESSED_BIB = EN_ACCESSED_BIB;
        break;

    case 2:
        COMMA = EN_COMMA;
        IN = FR_IN;
        ED_BIB = FR_ED_BIB;
        ED_CIT = FR_ED_CIT;
        EDNUM = FR_EDNUM;
        VOLNUM = FR_VOLNUM;
        NUM = FR_NUM;
        AND = FR_AND;
        TRANS_CIT = FR_TRANS_CIT;
        TRANS_BIB = FR_TRANS_BIB;
        OTHERS_CIT = FR_OTHERS_CIT;
        ACCESSED_CIT = FR_ACCESSED_CIT;
        ACCESSED_BIB = FR_ACCESSED_BIB;
        break;

    case 3:
        COMMA = AR_COMMA;
        IN = AR_IN;
        ED_CIT = AR_ED_BIB;
        ED_BIB = AR_ED_CIT;
        EDNUM = AR_EDNUM;
        VOLNUM = AR_VOLNUM;
        NUM = AR_NUM;
        AND = AR_AND;
        TRANS_CIT = AR_TRANS_CIT;
        TRANS_BIB = AR_TRANS_BIB;
        OTHERS_CIT = AR_OTHERS_CIT;
        ACCESSED_CIT = AR_ACCESSED_CIT;
        ACCESSED_BIB = AR_ACCESSED_BIB;
        break;
    }

    // format type for special field: italic, bold or underline
    switch (formatType) {
    case 1:
        FORM_OPEN = FORM_OPEN_I;		// italic
        FORM_CLOSE = FORM_CLOSE_I;
        break;
    case 2:
        FORM_OPEN = FORM_OPEN_B;		// bold
        FORM_CLOSE = FORM_CLOSE_B;
        break;
    case 3:
        FORM_OPEN = FORM_OPEN_U;		// underline
        FORM_CLOSE = FORM_CLOSE_U;
        break;
    default:
        FORM_OPEN = FORM_OPEN_I;		// italic
        FORM_CLOSE = FORM_CLOSE_I;
        break;
    }
}


/*************** BOOKS *****************/

void ChicagoFootnoteStyle::bookCitation()
{
//Barry Buran, Theory of Something, ed. Karen Fatima, trans. Mister Man (New York: Oxford Press, 2009), 34

    QString nameBlock;
    {
        if(elem->isAuthors() && elem->isEditors()){

            nameBlock.append(listCitationNames(elem->getAuthorsList(), "", "", 4));

            if(elem->getAuthorsListLength() > 3) { nameBlock.append(OTHERS_CIT); }

            nameBlock.append(elem->getTitle(COMMA + FORM_OPEN, FORM_CLOSE));
            if(elem->isVolumTitle()) {
                nameBlock.append(elem->getVolumNumber(COMMA + VOLNUM));
                nameBlock.append(elem->getVolumTitle(COMMA + FORM_OPEN, FORM_CLOSE));
            }

            nameBlock.append(listCitationNames(elem->getEditorsList(), COMMA+ED_CIT+" ", "", 4));
        }

        else if(elem->isEditors() && !elem->isAuthors()){
            nameBlock.append(listCitationNames(elem->getEditorsList(), "", COMMA + ED_CIT, 4));
            nameBlock.append(elem->getTitle(COMMA + FORM_OPEN, FORM_CLOSE));
            if(elem->isVolumTitle()) {
                nameBlock.append(elem->getVolumNumber(COMMA + VOLNUM));
                nameBlock.append(elem->getVolumTitle(COMMA + FORM_OPEN, FORM_CLOSE));
            }
        }

        else if(elem->isAuthors() && !elem->isEditors()){
            nameBlock.append(listCitationNames(elem->getAuthorsList(), "", COMMA, 4));
            if(elem->getAuthorsListLength() > 4) { nameBlock.append(OTHERS_CIT); }
            nameBlock.append(elem->getTitle(FORM_OPEN, FORM_CLOSE));
            if(elem->isVolumTitle()) {
                nameBlock.append(elem->getVolumNumber(COMMA + VOLNUM));
                nameBlock.append(elem->getVolumTitle(COMMA + FORM_OPEN, FORM_CLOSE));
            }
        }
        else{		// if the author and editor are not defined, make the title go first
            nameBlock.append(elem->getTitle(FORM_OPEN, FORM_CLOSE));
            if(elem->isVolumTitle()) {
                nameBlock.append(elem->getVolumNumber(COMMA + VOLNUM));
                nameBlock.append(elem->getVolumTitle(COMMA + FORM_OPEN, FORM_CLOSE));
            }
        }

        if(elem->isTranslators()){
            nameBlock.append(listCitationNames(elem->getTranslatorsList(), COMMA+TRANS_CIT, "", 4));
        }
    }

    // editions number and volumes
    {
        // edition number
        if(language == 3){
            nameBlock.append(elem->getEditionNumber(COMMA + EDNUM));
        }
        else{
            nameBlock.append(elem->getEditionNumber(COMMA, EDNUM));
        }

        // volum number
        if(!elem->isVolumTitle()) { nameBlock.append(elem->getVolumNumber(COMMA + VOLNUM)); }
    }


    nameBlock.append(" ");

    QString secondBlock;
    {
        secondBlock.append(elem->getLocation());

        if(elem->isLocation() && elem->isEditionsHome()) { secondBlock.append(": "); }

        secondBlock.append(elem->getEditionsHome());

        if((elem->isEditionsHome() || elem->isLocation()) && elem->isYearOfPublication()) { secondBlock.append(COMMA); }
        secondBlock.append(elem->getYearOfPublication());

        secondBlock.prepend("(").append(")");

        secondBlock.append(elem->getCurrentPage(COMMA));
        secondBlock.append(elem->getWebSiteUrl(COMMA));
        secondBlock.append(".");
    }

    citation = nameBlock + secondBlock;
}


void ChicagoFootnoteStyle::bookBibliography()
{
////Buzan, Barry. Theory of Something, ed. Karen Fatima, trans. Mister Man (New York: Oxford Press, 2009), 34
    QString nameBlock;
    {
        if(elem->isAuthors() && elem->isEditors()){

            nameBlock.append(listBibliographyNames(elem->getAuthorsList(), "", "", 10));
            nameBlock.append(elem->getTitle(" " + FORM_OPEN, FORM_CLOSE));
            if(elem->isVolumNumber() && elem->isVolumTitle()){
                nameBlock.append(elem->getVolumNumber(". " + VOLNUM));
                nameBlock.append(elem->getVolumTitle(". " + FORM_OPEN, FORM_CLOSE));
            }
            nameBlock.append(listCitationNames(elem->getEditorsList(), ". " +ED_BIB+" "));
        }
        else {
            if(elem->isEditors() && !elem->isAuthors()){
                nameBlock.append(listBibliographyNames(elem->getEditorsList(), "", " " + ED_CIT, 10));
                nameBlock.append(elem->getTitle(" " + FORM_OPEN, FORM_CLOSE));
            }
            else if(elem->isAuthors() && !elem->isEditors()){
                nameBlock.append(listBibliographyNames(elem->getAuthorsList(), "", "", 10));
                nameBlock.append(elem->getTitle(" " + FORM_OPEN, FORM_CLOSE));
            }
            else{		// if the author and editor are not defined, make the title go first
                nameBlock.append(elem->getTitle(FORM_OPEN, FORM_CLOSE));
            }

            if(elem->isVolumNumber() && elem->isVolumTitle()){
                nameBlock.append(elem->getVolumNumber(". " + VOLNUM));
                nameBlock.append(elem->getVolumTitle(". " + FORM_OPEN, FORM_CLOSE));
            }
        }

        nameBlock.append(listCitationNames(elem->getTranslatorsList(), ". " + TRANS_BIB, "", 10));
    }

    // editions number and volumes
    {
        if(language == 3){
            nameBlock.append(elem->getEditionNumber(". " + EDNUM));
            if(!elem->isVolumTitle()) { nameBlock.append(elem->getVolumNumber(". " + VOLNUM)); }
        }
        else{
            if(elem->isEditionNumber()) { nameBlock.append(". "); }
            nameBlock.append(elem->getEditionNumber("", EDNUM));
            if(elem->isVolumNumber() && !elem->isEditionNumber() && !elem->isVolumTitle()) { nameBlock.append("."); }
            nameBlock.append(elem->getVolumNumber(" " + VOLNUM));
        }
    }

    nameBlock = nameBlock.simplified();
    if(!nameBlock.endsWith(".")){ nameBlock.append(". "); }

    QString secondBlock = " ";
    {
        secondBlock.append(elem->getLocation());

        if(elem->isLocation() && elem->isEditionsHome()) { secondBlock.append(": "); }
        secondBlock.append(elem->getEditionsHome());

        if((elem->isEditionsHome() || elem->isLocation()) && elem->isYearOfPublication()) { secondBlock.append(COMMA); }
        secondBlock.append(elem->getYearOfPublication());

        if( (!secondBlock.endsWith(". ") || !secondBlock.endsWith("."))) { secondBlock.append(". "); }

        secondBlock.append(elem->getWebSiteUrl("", "."));
    }

    bibliography = nameBlock + secondBlock;
    bibliography = bibliography.simplified();
}


/********** 	BOOK'S CHAPTER ****************/

void ChicagoFootnoteStyle::chapterCitation()
{
//    Robert Keohane, “The Demand for International Regimes,’’
//    in International Regimes, ed. Stephen Krasner, 55–67
//    (Ithaca, NY: Cornell University Press, 1983).

    QString nameBlock;
    {
        if(elem->isAuthors() && elem->isEditors()){
            nameBlock.append(listCitationNames(elem->getAuthorsList(), "", "", 4));
            if(elem->getAuthorsListLength() > 4) { nameBlock.append(OTHERS_CIT); }

            nameBlock.append(elem->getChapter(COMMA + "\"", "\"" + COMMA));
            nameBlock.append(elem->getTitle(IN + FORM_OPEN, FORM_CLOSE));
            if(elem->isVolumTitle()) {
                nameBlock.append(elem->getVolumNumber(COMMA + VOLNUM));
                nameBlock.append(elem->getVolumTitle(COMMA + FORM_OPEN, FORM_CLOSE));
            }

            nameBlock.append(listCitationNames(elem->getEditorsList(), COMMA + ED_CIT + " "));
        }
        else{

            if(elem->isEditor() && !elem->isAuthor()){
                nameBlock.append(listCitationNames(elem->getEditorsList(), "", "", 4));
                nameBlock.append(COMMA + ED_CIT);
                nameBlock.append(elem->getChapter(COMMA + "\"", "\""));
                nameBlock.append(elem->getTitle(" " + IN + FORM_OPEN, FORM_CLOSE));
            }

            else if(elem->isAuthor() && !elem->isEditor()){
                nameBlock.append(listCitationNames(elem->getAuthorsList(), "", "", 3));
                if(elem->getAuthorsListLength() > 4) { nameBlock.append(OTHERS_CIT); }
                nameBlock.append(elem->getChapter(COMMA + " \"", "\""));
                nameBlock.append(elem->getTitle(". " + IN + FORM_OPEN, FORM_CLOSE));
            }

            else{
                nameBlock.append(elem->getChapter("\"", "\""));
                nameBlock.append(elem->getTitle(COMMA + IN + FORM_OPEN, FORM_CLOSE));
            }


            if(elem->isVolumTitle()) {
                nameBlock.append(elem->getVolumNumber(COMMA + VOLNUM));
                nameBlock.append(elem->getVolumTitle(COMMA + FORM_OPEN, FORM_CLOSE));
            }
        }

        nameBlock.append(listCitationNames(elem->getTranslatorsList(), COMMA + TRANS_CIT, "", 4));
    }

    {	// edition number
        if(language == 3){
            nameBlock.append(elem->getEditionNumber(COMMA + EDNUM));
        }
        else{
            nameBlock.append(elem->getEditionNumber(COMMA, EDNUM));
        }

        // volum number
        if(!elem->isVolumTitle()) { nameBlock.append(elem->getVolumNumber(COMMA + VOLNUM)); }
    }

    QString secondBlock;
    {
        secondBlock.append(elem->getLocation());

        if(elem->isLocation() && elem->isEditionsHome()) { secondBlock.append(": "); }

        secondBlock.append(elem->getEditionsHome());

        if((elem->isEditionsHome() || elem->isLocation()) && elem->isYearOfPublication()) { secondBlock.append(COMMA); }
        secondBlock.append(elem->getYearOfPublication());

        secondBlock.prepend(" (").append(")");

        secondBlock.append(elem->getCurrentPage(COMMA));
        secondBlock.append(elem->getWebSiteUrl(COMMA));
        secondBlock.append(".");
    }

    citation = nameBlock + secondBlock;
}


void ChicagoFootnoteStyle::chapterBibliography()
{
    QString nameBlock;
    {
        if(elem->isAuthor() && elem->isEditor()){
            nameBlock.append(listBibliographyNames(elem->getAuthorsList()));

            nameBlock.append(elem->getChapter(" \"", "\""));
            nameBlock.append(elem->getTitle(". " + IN + FORM_OPEN, FORM_CLOSE));
            if(elem->isVolumNumber() && elem->isVolumTitle()){
                nameBlock.append(elem->getVolumNumber(". " + VOLNUM));
                nameBlock.append(elem->getVolumTitle(". " + FORM_OPEN, FORM_CLOSE));
            }

            nameBlock.append(listCitationNames(elem->getEditorsList(), ". " + ED_BIB + " "));
        }
        else{

            if(elem->isEditor() && !elem->isAuthor()){
                nameBlock.append(listBibliographyNames(elem->getEditorsList(), "", " " + ED_CIT, 10));
                nameBlock.append(elem->getChapter(" \"", "\""));
                nameBlock.append(elem->getTitle(". " + FORM_OPEN, FORM_CLOSE));
            }

            else if(elem->isAuthor() && !elem->isEditor()){
                nameBlock.append(listBibliographyNames(elem->getAuthorsList()));
                nameBlock.append(elem->getChapter(" \"", "\""));
                nameBlock.append(elem->getTitle(". " + IN + FORM_OPEN, FORM_CLOSE));
            }

            else{
                nameBlock.append(elem->getChapter("\"", "\""));
                nameBlock.append(elem->getTitle(". " + IN + FORM_OPEN, FORM_CLOSE));
            }

            if(elem->isVolumNumber() && elem->isVolumTitle()){
                nameBlock.append(elem->getVolumNumber(". " + VOLNUM));
                nameBlock.append(elem->getVolumTitle(". " + FORM_OPEN, FORM_CLOSE));
            }
        }

        nameBlock.append(listCitationNames(elem->getTranslatorsList(), ". " + TRANS_BIB));
        nameBlock = nameBlock.simplified();
    }


    // editions number and volumes
    {
        if(language == 3){
            nameBlock.append(elem->getEditionNumber(". " + EDNUM));
            if(!elem->isVolumTitle()) { nameBlock.append(elem->getVolumNumber(". " + VOLNUM)); }
        }
        else{
            if(elem->isEditionNumber()) { nameBlock.append(". "); }
            nameBlock.append(elem->getEditionNumber("", EDNUM));
            if(elem->isVolumNumber() && !elem->isEditionNumber() && !elem->isVolumTitle()) { nameBlock.append("."); }
            if(!elem->isVolumTitle()) { nameBlock.append(elem->getVolumNumber(" " + VOLNUM)); }
        }
    }

    nameBlock = nameBlock.simplified();
    if(!nameBlock.endsWith(".")){ nameBlock.append(". "); }


    QString secondBlock = " ";
    {
        secondBlock.append(elem->getLocation());

        if(elem->isLocation() && elem->isEditionsHome()) { secondBlock.append(": "); }

        secondBlock.append(elem->getEditionsHome());

        if((elem->isEditionsHome() || elem->isLocation()) && elem->isYearOfPublication()) { secondBlock.append(COMMA); }
        secondBlock.append(elem->getYearOfPublication());
        secondBlock.append(elem->getPages(". "));

        if( (!secondBlock.endsWith(". ") || !secondBlock.endsWith("."))) { secondBlock.append(". "); }

        secondBlock.append(elem->getWebSiteUrl("", "."));
    }

    bibliography = nameBlock + secondBlock;
}


// Scientific Article
void ChicagoFootnoteStyle::scientificArticleCitation()
{
    /*	author
     * 	article's title
     * 	journal
     * 	journal volume
     *  issue number
     * 	month(s)/season and year
     *  page
     * 	DOI
     * 	web site
     */

    QString nameBlock;
    {
        nameBlock.append(listCitationNames(elem->getAuthorsList(), "", "", 4));
        if(elem->getAuthorsListLength() > 4) { nameBlock.append(OTHERS_CIT); }
        if( elem->isAuthor()) { nameBlock.append(COMMA); }

        nameBlock.append(elem->getTitle("\"", "\""));

        nameBlock.append(elem->getJournal(COMMA + FORM_OPEN, FORM_CLOSE));
    }

    QString secondBlock;
    {
        secondBlock.append(elem->getVolumNumber(" " + FORM_OPEN, FORM_CLOSE));
        secondBlock.append(elem->getIssueNumberJournal(COMMA + NUM));
//        secondBlock.append(elem->getPublicationDate(" (", ")"));	// deprecated
        secondBlock.append(elem->getMonthAndYearOfPublication(" (", ")"));

        if(elem->isMonthAndYearOfPublication() && elem->isCurrentPage()) { secondBlock.append(": "); }
        else if(!elem->isMonthAndYearOfPublication() && elem->isCurrentPage()) { secondBlock.append(COMMA); }

        secondBlock.append(elem->getCurrentPage());

        if(elem->isDOI()){
            secondBlock.append(elem->getDOI(COMMA));
        }
        else{
            secondBlock.append(elem->getWebSiteUrl(COMMA));
        }

        secondBlock.append(".");
    }

    citation = nameBlock + secondBlock;
}


void ChicagoFootnoteStyle::scientificArticleBibliography()
{
    QString nameBlock;
    {
        nameBlock.append(listBibliographyNames(elem->getAuthorsList()));
        nameBlock.append(elem->getTitle(" \"", "\""));
        nameBlock.append(elem->getJournal(". " + FORM_OPEN, FORM_CLOSE));
    }

    QString secondBlock;
    {
        secondBlock.append(elem->getVolumNumber(" " + FORM_OPEN, FORM_CLOSE));
        secondBlock.append(elem->getIssueNumberJournal(". " + NUM));
        secondBlock.append(elem->getMonthAndYearOfPublication(" (", ")"));

        if(elem->isMonthAndYearOfPublication() && elem->isPages()) { secondBlock.append(": "); }
        else if(!elem->isMonthAndYearOfPublication() && elem->isPages()) { secondBlock.append(". "); }

        secondBlock.append(elem->getPages());

        if(elem->isDOI()){
            secondBlock.append(elem->getDOI(". "));
        }
        else{
            secondBlock.append(elem->getWebSiteUrl(". "));
        }

        secondBlock.append(".");
    }

    bibliography = nameBlock + secondBlock;
}


void ChicagoFootnoteStyle::newspaperArticleCitation()
{
    QString nameBlock;
    {
        nameBlock.append(listCitationNames(elem->getAuthorsList(), "", "", 4));
        if(elem->getAuthorsListLength() > 4) { nameBlock.append(OTHERS_CIT); }
        if( elem->isAuthor()) { nameBlock.append(COMMA); }

        nameBlock.append(elem->getTitle("\"", "\""));

        nameBlock.append(elem->getJournal(COMMA + FORM_OPEN, FORM_CLOSE));
    }


    QString secondBlock;
    {
        secondBlock.append(elem->getPublicationDate(COMMA));
        secondBlock.append(elem->getWebSiteUrl(COMMA));

        secondBlock.append(".");
    }

    citation = nameBlock + secondBlock;
}


void ChicagoFootnoteStyle::newspaperArticleBibliography()
{
    QString nameBlock = "";
    {
        nameBlock.append(listBibliographyNames(elem->getAuthorsList()));
        nameBlock.append(elem->getTitle(" \"", "\""));
        nameBlock.append(elem->getJournal(". " + FORM_OPEN, FORM_CLOSE));
    }

    QString secondBlock = "";
    {
        secondBlock.append(elem->getPublicationDate(". "));
        secondBlock.append(elem->getWebSiteUrl(". "));


        secondBlock.append(".");
    }

    bibliography = nameBlock + secondBlock;
}


void ChicagoFootnoteStyle::dissertationCitation()
{
    QString first;
    {
        first.append(listCitationNames(elem->getAuthorsList(), "", "", 4));
        if(elem->getAuthorsListLength() > 4) { first.append(OTHERS_CIT); }
        if(elem->isAuthors()) { first.append(COMMA); }

        first.append(elem->getTitle("\"", "\""));
    }

    QString second;
    {
        second.append(elem->getDissertationType());
        if(elem->isDissertationType()) { second.append(COMMA); }
        second.append(elem->getInstitution());
        second.append(elem->getYearOfPublication(COMMA));

        second.prepend(" (").append(")");

        second.append(elem->getCurrentPage(COMMA));
        second.append(elem->getWebSiteUrl(". "));
    }

    citation = first + second + ".";
}

void ChicagoFootnoteStyle::dissertationBibliography()
{
    QString first;
    {
        first.append(listBibliographyNames(elem->getAuthorsList()));
        first.append(elem->getTitle(" \"", "\"."));
    }

    QString second = " ";
    {
        second.append(elem->getDissertationType());
        if(elem->isDissertationType()) { second.append(COMMA); }
        second.append(elem->getInstitution());
        second.append(elem->getYearOfPublication(COMMA));

        second.append(elem->getWebSiteUrl(". "));
    }

    bibliography = QString(first + second + ".").simplified();

}


void ChicagoFootnoteStyle::webSiteCitation()
{
    QString firstBlock = "";
    {
        firstBlock.append(listCitationNames(elem->getAuthorsList(), "", "", 4));
        if(elem->getAuthorsListLength() > 4) { firstBlock.append(OTHERS_CIT); }
        if( elem->isAuthors()) { firstBlock.append(COMMA); }

        firstBlock.append(elem->getTitle(" \"", "\""));
    }

    QString secondBlock = "";
    {
        secondBlock.append(elem->getWebSiteName(COMMA + FORM_OPEN, FORM_CLOSE));
        secondBlock.append(elem->getPublicationDate(COMMA));
        secondBlock.append(elem->getWebSiteUrl(COMMA));
        secondBlock.append(elem->getAccessDate(COMMA + "(" + ACCESSED_CIT, ")"));
    }

    citation = firstBlock + secondBlock + ".";
}


void ChicagoFootnoteStyle::webSiteBibliography()
{
    QString firstBlock = "";
    {
        firstBlock.append(listBibliographyNames(elem->getAuthorsList()));

        firstBlock.append(elem->getTitle(" \"", "\""));
    }

    QString secondBlock = "";
    {
        secondBlock.append(elem->getWebSiteName(". " + FORM_OPEN, FORM_CLOSE));
        secondBlock.append(elem->getPublicationDate(". "));
        secondBlock.append(elem->getWebSiteUrl(". "));
        secondBlock.append(elem->getAccessDate(". (" + ACCESSED_CIT, ")"));
    }

    bibliography = firstBlock + secondBlock + ".";
}



// treate a list of names and return a formated string which contains all the names
QString ChicagoFootnoteStyle::listCitationNames(QList<NameComponents> lst, QString pre, QString post, int maximumNumberOfNames) const
{
    if(lst.isEmpty()) { return QString(""); }
    QString formAddedNames;

    int i = 0;
    while( i < lst.length() ){
        if(i == maximumNumberOfNames){ break; }

        if(i == lst.length()-1 && 1 < lst.size()){		// put an "and" before the last name.
            formAddedNames.append(AND);
        }
        else if(i != 0) { formAddedNames.append(COMMA); }

        formAddedNames.append(DocumentInfo::toTitleCase(singleCitationName(lst[i])));

        i++;
    }

    return formAddedNames.prepend(pre).append(post);
}


QString ChicagoFootnoteStyle::listBibliographyNames(QList<NameComponents> lst, QString pre, QString post, int maximumNumberOfNames) const
{
    if(lst.isEmpty()) { return QString(""); }
    QString formAddedNames;

    int i = 0;
    while( i < lst.length() ){
        if(i == maximumNumberOfNames){ break; }

        if(i == lst.length()-1 && 1 < lst.size()){		// put an "and" before the last name.
            formAddedNames.append(AND);
        }
        else if(i != 0) { formAddedNames.append(COMMA); }

        if(i == 0){	// only the first name will be formated differentely
            formAddedNames.append(DocumentInfo::toTitleCase(singleBibliographyName(lst[i])));
        }
        else{
            formAddedNames.append(DocumentInfo::toTitleCase(singleCitationName(lst[i])));
        }

        i++;
    }
    if(!formAddedNames.endsWith(".")) { formAddedNames.append("."); }

    return formAddedNames.prepend(pre).append(post);
}


QString ChicagoFootnoteStyle::singleCitationName(NameComponents n, QString pre, QString post) const
{
    if(!n.isValid()) { return QString(""); }

    QString tmp_firstName;
    QString tmp_middleName;

    if(n.isFirstName()){
        tmp_firstName = n.getFirstName("", " ");
    }
    if(n.isMiddleName()) {
        tmp_middleName = n.getMiddleName().at(0).toUpper();
        tmp_middleName.append(". ");
    }

    QString nameStr = tmp_firstName + tmp_middleName + n.getFamilyName();
    nameStr = nameStr.simplified();

    if(nameStr.isEmpty()) { return QString(""); }
    else{ return nameStr.prepend(pre).append(post); }
}

QString ChicagoFootnoteStyle::singleBibliographyName(NameComponents n, QString pre, QString post) const
{
    if(!n.isValid()) { return QString(""); }

    QString tmp_firstName;
    QString tmp_middleName;
    QString tmp_familyName;

    if(n.isFamilyName()){
        tmp_familyName = n.getFamilyName("", COMMA);
    }

    if(n.isFirstName()){
        tmp_firstName = n.getFirstName();
    }

    if(n.isMiddleName()){
        tmp_middleName = n.getMiddleName().at(0).toUpper();
        tmp_middleName.prepend(" ");
    }

    QString biblio = tmp_familyName + tmp_firstName + tmp_middleName;
    biblio.append(".");		// end with a period anyway

    return biblio.simplified().prepend(pre).append(post);
}

ChicagoFootnoteStyle::~ChicagoFootnoteStyle()
{
}





