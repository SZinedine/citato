#include "namewidget.h"

NameWidget::NameWidget(int lang, int Xth, NameWidget::Person person, int labelMinimumWidth, QWidget *parent) : QWidget(parent)
{
    lab = new QLabel;
//    lab->setText(labelString);
    lab->setMinimumWidth(labelMinimumWidth);
    per = person;
    n = Xth;

    firstName = new QLineEdit;
    middleName = new QLineEdit;
    familyName = new QLineEdit;

    firstName->setAcceptDrops(false);
    middleName->setAcceptDrops(false);
    familyName->setAcceptDrops(false);

    layout = new QHBoxLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);
    setContentsMargins(0, 0, 0, 0);

    layout->addWidget(lab);
    layout->addWidget(firstName);
    layout->addWidget(middleName);
    layout->addWidget(familyName);

    adaptLanguage(lang);	// adapt language of place holders
}

void NameWidget::adaptLanguage(int language)
{
    // set the text holder following the language
    QString str_first;
    QString str_middle;
    QString str_family;

    QString auth, ed, trans;

    switch (language) {
    case 1:
        str_first = "First Name";
        str_middle = "Middle Name";
        str_family = "Family Name";

        auth = "Author ";
        ed = "Editor ";
        trans = "Translator ";
        setLayoutDirection(Qt::LeftToRight);
        break;
    case 2:
        str_first = "Prénom";
        str_middle = "Deuxième Prénom";
        str_family = "Nom";

        auth = "Auteur ";
        ed = "Éditeur ";
        trans = "Traducteur ";
        setLayoutDirection(Qt::LeftToRight);
        break;
    case 3:
        str_first = "الإسم";
        str_middle = "الإسم الثاني";
        str_family = "اللقب";

        auth = "الكاتب ";
        ed = "الناشر ";
        trans = "المترجم ";
        setLayoutDirection(Qt::RightToLeft);
        break;
    default:
        str_first = "ERROR";
        str_middle = "ERROR";
        str_family = "ERROR";
        auth = "Author ERROR ";
        ed = "Editor ERROR ";
        trans = "Translator ERROR ";
        break;
    }

    firstName->setPlaceholderText(str_first);
    middleName->setPlaceholderText(str_middle);
    familyName->setPlaceholderText(str_family);

    // put a number after a name in the label
    // only if it isn't the first widget (the first author)
    if(n != 1){
        if(per == Person::author) { lab->setText(auth + QString::number(n)); }
        else if(per == Person::editor) { lab->setText(ed + QString::number(n)); }
        else if(per == Person::translator) { lab->setText(trans + QString::number(n)); }
    }
    else{
        if(per == Person::author) { lab->setText(auth); }
        else if(per == Person::editor) { lab->setText(ed); }
        else if(per == Person::translator) { lab->setText(trans); }

    }
}

void NameWidget::setName(NameComponents name)
{
    firstName->setText(name.getFirstName());
    middleName->setText(name.getMiddleName());
    familyName->setText(name.getFamilyName());
}

NameComponents NameWidget::getName()
{
    NameComponents nameComponents;
    nameComponents.setFirstName(firstName->text());
    nameComponents.setMiddleName(middleName->text());
    nameComponents.setFamilyName(familyName->text());

    return nameComponents;
}


void NameWidget::clear()
{
    firstName->clear();
    middleName->clear();
    familyName->clear();
}


