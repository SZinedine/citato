#ifndef ABSTRACTSTYLE_H
#define ABSTRACTSTYLE_H

#include <QString>
#include "documentinfo.h"

class AbstractStyle
{
public:
    AbstractStyle(DocumentInfo &comp, int documentType, int lang, int format);
    virtual ~AbstractStyle() { delete elem; }

    inline QString getCitation() { return citation.simplified(); }
    inline QString getBibliography() { return bibliography.simplified(); }
    virtual QString listCitationNames(QList<NameComponents> lst, QString pre, QString post, int maximumNumberOfNames) const = 0;
    virtual QString listBibliographyNames(QList<NameComponents> lst, QString pre, QString post, int maximumNumberOfNames) const = 0;
    virtual QString singleCitationName(NameComponents n, QString pre="", QString post="") const =0;
    virtual QString singleBibliographyName(NameComponents n, QString pre="", QString post="") const = 0;

protected:
    virtual void adaptLanguage(){}				// adapt particles language (like latin/arabic commas...)
    void callAppropriateDocument();		// call the right member function following the selected document

    virtual void bookCitation() = 0;							// book
    virtual void bookBibliography() = 0;
    virtual void chapterCitation() = 0;						// chapter of a book
    virtual void chapterBibliography() = 0;
    virtual void scientificArticleCitation() = 0;				// scientific article (from a journal)
    virtual void scientificArticleBibliography() = 0;
    virtual void newspaperArticleCitation() = 0;				// article from a newspaper
    virtual void newspaperArticleBibliography() = 0;
    virtual void dissertationCitation() = 0;					// dissertation
    virtual void dissertationBibliography() = 0;
    virtual void webSiteCitation() = 0;						// web site
    virtual void webSiteBibliography() = 0;

    // particles
    QString COMMA;		// host one of the commas following the selected language
    QString IN;			// in, dans, "fi"
    QString AND;		// and, et, "wa"
    QString ED_CIT;		// ed., ed., "moharir"
    QString ED_BIB;		// edited by, édité par, "moharir"
    QString EDNUM;		// the edition number
    QString VOLNUM;		// the volum number
    QString NUM;		// no. "raqm"
    QString TRANS_CIT;
    QString TRANS_BIB;
    QString OTHERS_CIT;	// et al. "wa akharoun"
    QString ACCESSED_CIT;	// accessed, consulté le,
    QString ACCESSED_BIB;
    QString FORM_OPEN;	// <i> <b> <u>
    QString FORM_CLOSE;

    DocumentInfo *elem;	// contains the input text and a convinient interface to retrieve them
    int document;		// document type: book, chapter, article...
    int language;
    int formatType;		// italics, bold...
    QString citation;		// store the final result
    QString bibliography;

};

#endif // ABSTRACTSTYLE_H
