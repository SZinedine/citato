#include "namecomponents.h"

NameComponents::NameComponents()
{

}

NameComponents::NameComponents(QString fir, QString mid, QString fam)
{
    firstName = fir;
    middleName = mid;
    familyName = fam;
}

/********************************************/
/*************** SETTERS ********************/
/********************************************/

void NameComponents::setNames(QString first, QString middle, QString family){
    firstName = first.simplified();
    middleName = middle.simplified();
    familyName = family.simplified();
}

void NameComponents::setFirstName(QString first) {
    firstName = first.simplified();
}

void NameComponents::setMiddleName(QString middle) {
    middleName = middle.simplified();
}

void NameComponents::setFamilyName(QString family) {
    familyName = family.simplified();
}



/********************************************/
/*************** GETTERS ********************/
/********************************************/

QString NameComponents::getFirstName(QString pre, QString post){
    if(isFirstName()) {
        QString x = firstName;
        return x.prepend(pre).append(post);
    }
    else { return QString(""); }
}


QString NameComponents::getMiddleName(QString pre, QString post){
    if(isMiddleName()){
        QString x = middleName;
        return x.prepend(pre).append(post);
    }
    else { return QString(""); }
}


QString NameComponents::getFamilyName(QString pre, QString post){
    if(isFamilyName()){
        QString x = familyName;
        return x.prepend(pre).append(post);
    }
    else { return QString(""); }
}


QString NameComponents::getCitation(QString pre, QString post){
    if(!isValid()) { return QString(""); }

    QString tmp_firstName;
    QString tmp_middleName;

    if(isFirstName()){
        tmp_firstName = getFirstName("", " ");
    }
    if(isMiddleName()) {
        tmp_middleName = getMiddleName().at(0).toUpper();
        tmp_middleName.append(". ");
    }

    QString citation = tmp_firstName + tmp_middleName + getFamilyName();
    citation = citation.simplified();

    if(citation.isEmpty()) { return QString(""); }
    else{ return citation.prepend(pre).append(post); }
}



QString NameComponents::getBibliography(QString COMMA, QString pre, QString post){
    if(!isValid()) { return QString(""); }

    QString tmp_firstName;
    QString tmp_middleName;
    QString tmp_familyName;

    if(isFamilyName()){
        tmp_familyName = getFamilyName("", COMMA);
    }

    if(isFirstName()){
        tmp_firstName = getFirstName();
    }

    if(isMiddleName()){
        tmp_middleName = getMiddleName().at(0).toUpper();
        tmp_middleName.prepend(" ");
    }

    QString biblio = tmp_familyName + tmp_firstName + tmp_middleName;
    biblio.append(".");		// end with a period anyway

    return biblio.simplified().prepend(pre).append(post);
}



/********************************************/
/*************** TESTERS ********************/
/********************************************/


bool NameComponents::isValid() const{
    if(isFirstName() || isFamilyName()){
        return true;
    }
    else { return false; }
}


bool NameComponents::isFirstName() const {
    if(!firstName.isEmpty()){ return true; }
    else { return false; }
}

bool NameComponents::isMiddleName() const{
    if(!middleName.isEmpty()) { return true; }
    else { return false; }
}

bool NameComponents::isFamilyName() const {
    if(!familyName.isEmpty()) { return true; }
    else { return false; }
}
