#ifndef CITATOMANAGERGUI_H
#define CITATOMANAGERGUI_H

#include <QMainWindow>
#include <QPushButton>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QMenu>
#include <QAction>
#include <QFileDialog>
#include <QDir>
#include <QString>
#include <QStringList>
#include <QMap>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QStringList>

#include "citatodatabase.h"
#include "citatofile.h"
#include "documentinfo.h"


class CitatoManagerGui : public QMainWindow
{
    Q_OBJECT
public:
    explicit CitatoManagerGui(QWidget *parent = nullptr);

    void setupCentral();
    void setupMenu();
    void setupLayout();
    void setupSignalsAndSlots();
    void addItemToTreeWidget(QJsonDocument *db, const QString &filename);
    void addItemToTreeWidget(QFile *dbFile);

public slots:
//    void loadDatabase();		// deprecated
    void openDatabase();	// add a database to the TreeWidget
    void createNewDatabase();

signals:
    void documentInfoReady(DocumentInfo documentInfo);

private:
    QWidget *mainWidget;
    DocumentInfo *documentInfo;
    QTreeWidget *containerWidget;
    QMap<QString, QJsonDocument*> dbItems;	// stores the jsonDocument db and its path
    QPushButton *importDatabaseButton;
    QPushButton *exportDatabaseButton;
    QPushButton *addFileToDatabaseButton;
    QPushButton *addItemFromFileButton;
    QPushButton *removeItemFromDatabaseButton;
    QPushButton *createNewDatabaseButton;
    QPushButton *quitButton;
    QGridLayout *mainLayout;

};

#endif // CITATOMANAGERGUI_H
