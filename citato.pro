#-------------------------------------------------
#
# Project created by QtCreator 2018-02-04T21:08:02
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = citato
TEMPLATE = app
RC_ICONS = images/citato.ico

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

include(3rdParty/QSimpleUpdater/QSimpleUpdater.pri)

RESOURCES += \
    citato.qrc \
    3rdParty/QDarkStyleSheet/qdarkstyle/style.qrc

HEADERS += \
    src/documentinfo.h \
    src/citatogui.h \
    src/namecomponents.h \
    src/namewidget.h \
    src/chicagofootnotestyle.h \
    src/testdocument.h \
    src/citatomanagergui.h \
    src/citatofile.h \
    src/citatodatabase.h \
    src/chicagointextstyle.h \
    src/abstractstyle.h \
    src/particles.h \
    src/field.h

SOURCES += \
    src/documentinfo.cpp \
    src/main.cpp \
    src/citatogui.cpp \
    src/namecomponents.cpp \
    src/namewidget.cpp \
    src/chicagofootnotestyle.cpp \
    src/testdocument.cpp \
    src/citatomanagergui.cpp \
    src/citatofile.cpp \
    src/citatodatabase.cpp \
    src/chicagointextstyle.cpp \
    src/abstractstyle.cpp \
    src/field.cpp

