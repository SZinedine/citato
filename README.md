# Citato
Citato is a citation style composer. It helps the user put together his academic citations and bibliography entries.  
Citato is originally made to give descent support to Arabic citations. but it also supports English and French.  
Citato is under heavy development, it currently supports only some document types from the Chicago Style.  
